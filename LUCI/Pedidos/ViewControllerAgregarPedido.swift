//
//  ViewControllerAgregarPedido.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 15/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit
import CoreData

class ViewControllerAgregarPedido: UIViewController {
    
    // Outlets
    @IBOutlet weak var clientesPicker: UIPickerView!
    @IBOutlet weak var itemsPicker: UIPickerView!
    @IBOutlet weak var cantidadLabel: UILabel!
    @IBOutlet weak var cantidadStepper: UIStepper!
    @IBOutlet weak var carritoTableView: UITableView!
    @IBOutlet weak var itemImageView: UIImageView!
    
    let usuario = UserDefaults.standard.string(forKey: "usuario")!
    
    let myImages:[UIImage?] = [UIImage(named: "User 1"), UIImage(named: "User 2"), UIImage(named: "User 3"), UIImage(named: "User 4"), UIImage(named: "User 5"), UIImage(named: "User 6"), UIImage(named: "User 7"), UIImage(named: "User 8"), UIImage(named: "User 9"), UIImage(named: "User 10")]
    
    //Items
    let items:[String] = ["Aguacate","Manzana","Pera","Mango","Papa","Guayaba","Coco","Durazno"]
    
    let itemsImages: [String: UIImage?] = ["Aguacate":UIImage(named: "aguacate"),"Manzana":UIImage(named: "manzana"),"Pera":UIImage(named: "pera"),"Mango":UIImage(named: "mango"),"Papa":UIImage(named: "papa"),"Guayaba":UIImage(named: "guayaba"),"Coco":UIImage(named: "coco"),"Durazno":UIImage(named: "durazno")]
    
    //Carrito
    
    var itemsCarrito = [String]()
    var cantidadItemsCarrito = [String:Int]()
    var telefonoCliente = 0
    var itemSeleccionado = "Aguacate"
    var cantidadSeleccionada = 1
    
    var setDomiciliarios = [Domiciliario]()
    
    //Clientes DB
    var setClientes = [Cliente]()
    var clienteSeleccionado: Cliente?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tutorialCompletado = UserDefaults.standard.bool(forKey: "tutorialCompletado")
        if tutorialCompletado == false{
            UserDefaults.standard.set(4, forKey:"tutoCount")
            let tutorial = UIAlertController(title: "Tutorial",
            message: "Acá puedes agregar un nuevo pedido. Por favor llena los campos correctamente y presiona el botón finalizar pedido.",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                       style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            tutorial.addAction(ok)
            //Muestra la alerta
            present(tutorial,
            animated: true,
            completion: nil)
        }
        
        
        //ClientePickerView
        clientesPicker.dataSource = self
        clientesPicker.delegate = self
        clientesPicker.setValue(UIColor.black, forKeyPath: "textColor")
        //ItemsPickerView
        itemsPicker.dataSource = self
        itemsPicker.delegate = self
        itemsPicker.setValue(UIColor.black, forKeyPath: "textColor")
        //CantidadStepper
        cantidadStepper.minimumValue = 1
        cantidadStepper.maximumValue = 30
        cantidadLabel.text=String(Int(cantidadStepper.value))
        //CarritoTableView
        carritoTableView.dataSource = self
        carritoTableView.tableFooterView = UIView()
        carritoTableView.register(UINib(nibName: "CarritoTableViewCell", bundle: nil), forCellReuseIdentifier: "carritoTVC")
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setDomiciliarios = [Domiciliario]()
        // Consulta Clientes
        setClientes = [Cliente]()
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
        let result = try? managedObjectContext.fetch(fetchRequest)
        let resultData = result as! [Cliente]
        for object in resultData{
            setClientes.append(object)
        }
        clienteSeleccionado = setClientes[0]
        //Consulta Domiciliarios
        let fetchRequest2 = NSFetchRequest<NSFetchRequestResult>(entityName: "Domiciliario")
        let result2 = try? managedObjectContext.fetch(fetchRequest2)
        let resultData2 = result2 as! [Domiciliario]
        for object2 in resultData2{
            setDomiciliarios.append(object2)
        }
        
        
        
    }
    @IBAction func cantidadStepperAction(_ sender: Any) {
        cantidadLabel.text = String (Int(cantidadStepper.value))
        cantidadSeleccionada = Int(cantidadStepper.value)
    }
    @IBAction func agregarAlCarritoAction(_ sender: Any) {
        itemsCarrito.append(itemSeleccionado)
        cantidadItemsCarrito[itemSeleccionado] = cantidadSeleccionada
        carritoTableView.reloadData()
    }
    @IBAction func guardarPedido(_ sender: Any) {
        if itemsCarrito.count==0{
            let noItems = UIAlertController(title: "Sin items",
                                            message: "Debes seleccionar al menos un item.",
                                            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                   style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            noItems.addAction(ok)
            //Muestra la alerta
            present(noItems,
                    animated: true,
                    completion: nil)
        }else{
            
            if setDomiciliarios.count != 0{
                let rand = arc4random_uniform(UInt32(setDomiciliarios.count))
                let domiciliario = setDomiciliarios[Int(rand)]
                let g = guardarPedidoDB(id: telefonoCliente,items: itemsCarrito, itemsCantidad: cantidadItemsCarrito, domi: domiciliario, usuario: usuario, cliente: clienteSeleccionado!)
                if g{
                    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                    subirPedidos(usuario: usuario, context: managedObjectContext)
                    let guardadoExitoso = UIAlertController(title: "Pedido Exitoso",
                                                            message: "El pedido ha sido creado.",
                                                            preferredStyle: .alert)
                    
                    //Creamos el UIAlertAction que nos permitirá volver
                    let volver = UIAlertAction(title: "Ok",
                                               style: .default) { (action: UIAlertAction) -> Void in
                                                self.navigationController?.popViewController(animated: true)
                    }
                    //Agrega el boton de volver a la alerta
                    guardadoExitoso.addAction(volver)
                    //Muestra la alerta
                    present(guardadoExitoso,
                            animated: true,
                            completion: nil)
                }
            }else{
                let noDomis = UIAlertController(title: "Sin domiciliarios",
                                                message: "No hay domiciliarios registrados.",
                                                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                       style: .default) { (action: UIAlertAction) -> Void in
                }
                //Agrega el boton de volver a la alerta
                noDomis.addAction(ok)
                //Muestra la alerta
                present(noDomis,
                        animated: true,
                        completion: nil)
                
            }
            
        }
    }
    
}

//MARK: - Guardar pedido en BD
func guardarPedidoDB(id:Int,items:[String],itemsCantidad:[String:Int], domi:Domiciliario, usuario:String, cliente:Cliente) -> Bool {
    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let pedido = Pedido(context: managedObjectContext)
    pedido.estado = 1
    pedido.fechaCreacion = Date()
    //pedido.fechaCreacion = Date(timeIntervalSinceNow: -345612)
    pedido.guardadoRemoto = false
    pedido.usuario = usuario
    for it in items{
        let item = Item(context: managedObjectContext)
        let cant = itemsCantidad[it]
        item.nombre = it
        item.cantidad = Int64(cant!)
        pedido.addToItems(item)
    }
    cliente.addToPedidos(pedido)
    print (cliente.nombre)
    domi.addToPedidos(pedido)
    //Agregar a firebase
    
    do {
        try managedObjectContext.save()
        print("saved!")
        return true
    } catch let error as NSError  {
        print("Could not save \(error), \(error.userInfo)")
        return false
    }
}


// MARK: -ClientesPickerView DataSource Delegate
extension ViewControllerAgregarPedido: UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var rows = 0
        if pickerView == self.clientesPicker{
            if setClientes.count == 0{
                rows = 1
            }else{
                rows = setClientes.count
            }
        }
        
        if pickerView == self.itemsPicker{
            if items.count == 0{
                rows = 1
            }else{
                rows = items.count
            }
            
        }
        
        return rows
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var lineText = " "
        if pickerView == self.clientesPicker{
            if setClientes.count == 0{
                lineText = "No hay clientes registrados"
            }
            else{
                let cliente = setClientes[row]
                let nombre = cliente.value(forKey: "nombre") as? String
                let apellido = cliente.value(forKey: "apellido") as? String
                lineText = "\(nombre!) \(apellido!)"
            }
        }
        if pickerView == self.itemsPicker{
            if items.count == 0{
                lineText = "No hay items registrados"
            }
            else{
                lineText = items[row]
            }
        }
        return lineText
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.itemsPicker{
            itemImageView.image = itemsImages[items[row]]!
            itemSeleccionado = items[row]
        }
        if pickerView == self.clientesPicker{
            clienteSeleccionado = setClientes[row]
            print(clienteSeleccionado?.nombre)
        }
    }
}


// MARK: -CarritoTableView DataSource
extension ViewControllerAgregarPedido: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsCarrito.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "carritoTVC") as? CarritoTableViewCell
        let nombreItem = itemsCarrito[indexPath.row]
        cell?.nombreLabel.text = nombreItem
        cell?.itemImageView.image = itemsImages[nombreItem]!
        cell?.cantidadLabel.text = String(cantidadItemsCarrito[nombreItem]!)
        return cell!
    }
}
