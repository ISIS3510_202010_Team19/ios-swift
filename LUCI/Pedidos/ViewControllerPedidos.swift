//
//  ViewController.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 11/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit
import CoreData

class ViewControllerPedidos: UIViewController{

    //Outlets
    @IBOutlet weak var clientesCollectionView: UICollectionView!
    @IBOutlet weak var pedidosTableView: UITableView!
    @IBOutlet weak var llamadaButton: UIButton!
    
    
    //Datos CORE Data
    var setClientes = [Cliente]()
    var setPedidos = [Pedido]()
    var pedidoSeleccionado: Pedido?
    var clienteSeleccionado: Cliente?
    var descargaDomiciliariosFinalizada = false
    var descargaClientesFinalizada = false
    var primeraVez = true
    
    let usuario = UserDefaults.standard.string(forKey: "usuario")!
    
    //Datos para mostrar usuarios
    private let myUsers = ["User 1", "User 2", "User 3", "User 4", "User 5", "User 6", "User 7", "User 8", "User 9", "User 10"]
    private let myImages:[UIImage?] = [UIImage(named: "User 1"), UIImage(named: "User 2"), UIImage(named: "User 3"), UIImage(named: "User 4"), UIImage(named: "User 5"), UIImage(named: "User 6"), UIImage(named: "User 7"), UIImage(named: "User 8"), UIImage(named: "User 9"), UIImage(named: "User 10")]
    
    //Variables
    private let screenWidth = UIScreen.main.bounds.width
    let cellSpacingHeight: CGFloat = 5
    var llamadas = getLlamadasFiles() as! [URL]
    
    //Datos para mostar pedidos
    private let myPedidos = ["User 1", "User 2", "User 3", "User 4", "User 5","User 6", "User 7", "User 8", "User 9", "User 10"]
    
    override func viewDidLoad() {
        // get current number of times app has been launched
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh), name: NSNotification.Name(rawValue: "newDataNotif"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadPedidos), name: NSNotification.Name(rawValue: "newDownloadFinished"), object: nil)
        //Clientes Collection View
        clientesCollectionView.backgroundColor = UIColor(named: "Base")
        clientesCollectionView.layer.cornerRadius = 10
        clientesCollectionView.dataSource = self
        clientesCollectionView.register(UINib(nibName: "ClienteFavCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "clienteCell")
        clientesCollectionView.delegate = self
        
        //Pedidos TableView
        pedidosTableView.dataSource = self
        pedidosTableView.tableFooterView = UIView()
        pedidosTableView.delegate = self
        pedidosTableView.register(UINib(nibName:  "PedidoTableViewCell", bundle: nil), forCellReuseIdentifier: "pedidoTVC")
        
    }
    
    @objc func loadPedidos(notification:NSNotification) {
        
        let mensaje = notification.userInfo!["data"] as! String
        if (mensaje == "domiciliariosFinalizado"){
            descargaDomiciliariosFinalizada = true
        } else {
            descargaClientesFinalizada = true
        }
        if (descargaClientesFinalizada == true && descargaDomiciliariosFinalizada == true && primeraVez == true)
        {
            let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            DispatchQueue.global().async{
                syncPedidos(usuario: self.usuario, context: managedObjectContext)
            }
            primeraVez = false
        }
    }
    
    @objc func refresh(notification:NSNotification) {
        setPedidos = [Pedido]()
        setClientes = [Cliente]()
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
        let result = try? managedObjectContext.fetch(fetchRequest)
        let resultData = result as! [Cliente]
        for object in resultData{
            if object.favorito{
                setClientes.append(object)
            }
            let pedidos = object.pedidos?.allObjects as! [Pedido]

            for p in pedidos{
                if (p.estado != 4){
                setPedidos.append(p)
                }
            }
        }
        clientesCollectionView.reloadData()
        pedidosTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let tutorialCompletado = UserDefaults.standard.bool(forKey: "tutorialCompletado")
        let estadoTuto = UserDefaults.standard.integer(forKey: "tutoCount")
        let empezarTutorial = UserDefaults.standard.bool(forKey: "empezarTutorial")
        // increment received number by one
        if empezarTutorial == true{
            let tutorial = UIAlertController(title: "Tutorial",
            message: "¿Deseas comenzar realizando el tutorial?",
            preferredStyle: .alert)
            let no = UIAlertAction(title: "No",
                                       style: .default) { (action: UIAlertAction) -> Void in
                                        UserDefaults.standard.set(true, forKey: "tutorialCompletado")
                                        UserDefaults.standard.set(false, forKey: "empezarTutorial")
            }
            //Creamos el UIAlertAction que nos permitirá volver
            let si = UIAlertAction(title: "Si",
                                       style: .default) { (action: UIAlertAction) -> Void in
                                        UserDefaults.standard.set(false, forKey: "empezarTutorial")
                                        let tutorial = UIAlertController(title: "Tutorial",
                                        message: "Acá puedes ver todos los pedidos que están en curso. Para crear un pedido debes primero crear un cliente y un domiciliario. Accede mediante la barra inferior a la parte de domiciliarios para crear uno nuevo.",
                                        preferredStyle: .alert)
                                        
                                        //Creamos el UIAlertAction que nos permitirá volver
                                        let ok = UIAlertAction(title: "Ok",
                                                                   style: .default) { (action: UIAlertAction) -> Void in
                                        }
                                        //Agrega el boton de volver a la alerta
                                        tutorial.addAction(ok)
                                        //Muestra la alerta
                                        self.present(tutorial,
                                        animated: true,
                                        completion: nil)
                                        
            }
            //Agrega el boton de volver a la alerta
            tutorial.addAction(no)
            tutorial.addAction(si)
            //Muestra la alerta
            present(tutorial,
            animated: true,
            completion: nil)
        }
        
        if tutorialCompletado == false{
            if estadoTuto==1{
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Acá puedes ver todos los pedidos que están en curso. Para crear un pedido debes primero crear un cliente y un domiciliario. Accede mediante la barra inferior a la parte de domiciliarios para crear uno nuevo.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            }
            else if estadoTuto == 3{
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Ahora que ya creaste un cliente y un domiciliario vamos a crear un pedido por voz presionando el icono del micrófono o manual presionando el ícono de más. Ambos situados en la parte superior",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            }else{
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Ahora que ya tienes un pedido creado presionalo para ver los detalles y poder continuar con el estado del pedido",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            }
            
            
        }
        llamadas = getLlamadasFiles() as! [URL]
        setPedidos = [Pedido]()
        setClientes = [Cliente]()
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
        let result = try? managedObjectContext.fetch(fetchRequest)
        let resultData = result as! [Cliente]
        for object in resultData{
            if object.favorito{
                setClientes.append(object)
            }
            let pedidos = object.pedidos?.allObjects as! [Pedido]

            for p in pedidos{
                if (p.estado != 4){
                setPedidos.append(p)
                }
            }
        }
        clientesCollectionView.reloadData()
        pedidosTableView.reloadData()
        
    }
    
    
    // Mostrar pantalla de llamada
    @IBAction func llamadaButonAction(_ sender: Any) {
        if setClientes.count==0{
            let sinClientes = UIAlertController(title: "Sin clientes",
            message: "No hay clientes registrados, por favor registra uno.",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                       style: .default) { (action: UIAlertAction) -> Void in
                                        self.navigationController?.popViewController(animated: true)
            }
            //Agrega el boton de volver a la alerta
            sinClientes.addAction(ok)
            //Muestra la alerta
            present(sinClientes,
            animated: true,
            completion: nil)
        }
        
        performSegue(withIdentifier: "VCLlamada", sender: self)
        
    }
    
    // Mostrar pantalla de agregar pedido manual
    @IBAction func agregarPedido(_ sender: Any) {
        if setClientes.count==0{
            let sinClientes = UIAlertController(title: "Sin clientes",
            message: "No hay clientes registrados, por favor registra uno.",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                       style: .default) { (action: UIAlertAction) -> Void in
                                        self.navigationController?.popViewController(animated: true)
            }
            //Agrega el boton de volver a la alerta
            sinClientes.addAction(ok)
            //Muestra la alerta
            present(sinClientes,
            animated: true,
            completion: nil)
        }
        
        var setDomiciliarios = [Domiciliario]()
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Domiciliario")
        let result = try? managedObjectContext.fetch(fetchRequest)
        let resultData = result as! [Domiciliario]
        for object in resultData{
            setDomiciliarios.append(object)
        }
        
        if setDomiciliarios.count==0{
            let sinClientes = UIAlertController(title: "Sin domiciliarios",
            message: "No hay domiciliarios registrados, por favor registra uno.",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                       style: .default) { (action: UIAlertAction) -> Void in
                                        self.navigationController?.popViewController(animated: true)
            }
            //Agrega el boton de volver a la alerta
            sinClientes.addAction(ok)
            //Muestra la alerta
            present(sinClientes,
            animated: true,
            completion: nil)
        }
        performSegue(withIdentifier: "VCAgregarPedido", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VCPedidoDetail"{
            if let destino = segue.destination as? PedidoDetailViewController{
                destino.pedido = pedidoSeleccionado!
            }
        }else if segue.identifier == "VClienteDetail"{
            if let destino = segue.destination as? ClienteDetailViewController{
                destino.cliente = clienteSeleccionado!
            }
        }
    }
    
    
}


//Get llamadas
func getLlamadasFiles() -> [Any]{
    let fileManager = FileManager.default
    let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
    do {
        let fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
        return fileURLs
    } catch {
        //print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        return []
    }
}






// MARK: - ClientesCollectionView DataSource
extension ViewControllerPedidos: UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return setClientes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "clienteCell", for: indexPath) as? ClienteFavCollectionViewCell
        
        cell?.nombreLabel.text = setClientes[indexPath.row].nombre!
        if setClientes[indexPath.row].foto != nil{
            cell?.clienteImage.image = UIImage(data: setClientes[indexPath.row].foto!)!
            cell?.clienteImage.hideActivityIndicator(activityIndicator: cell!.activityIndicator, backgroundView: cell!.fondo)
        }else{
            cell?.clienteImage.showActivityIndicator(activityIndicator: cell!.activityIndicator,backgroundView: cell!.fondo)
        }
        cell?.clienteImage.layer.cornerRadius = (cell?.clienteImage.frame.size.width)! / 2
        cell?.clienteImage.clipsToBounds = true
        return cell!
    }
}



//MARK: - ClientesCollectionView Delegate
extension ViewControllerPedidos: UICollectionViewDelegate{
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        clienteSeleccionado = setClientes[indexPath.row]
        performSegue(withIdentifier: "VClienteDetail", sender: self)
    }
}

//MARK: - PedidosTableView DataSource
extension ViewControllerPedidos: UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return getLlamadasFiles().count
        return setPedidos.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //LLAMADAS GRABADAS
        /*
        let llamada = llamadas[indexPath.row]
        let nombreArr = llamada.absoluteString.split(separator: "/")
        let nombre = nombreArr[nombreArr.count - 1].replacingOccurrences(of: "_", with: " ").replacingOccurrences(of: ".m4a", with: "").replacingOccurrences(of: "audio ", with: "")
        let cell = tableView.dequeueReusableCell(withIdentifier: "pedidoTVC") as? PedidoTableViewCell
        
        
        cell?.nombreClienteLabel.text = myPedidos[indexPath.row]
        cell?.horaPedidoLabel.text = nombre
        
        
        cell?.clienteImageView.image = myImages[indexPath.row]
        cell?.clienteImageView.layer.cornerRadius = (cell?.clienteImageView.frame.size.width)! / 2
        cell?.clienteImageView.clipsToBounds = true
        //cell?.nombreClienteLabel.text = myUsers[indexPath.row]
        return cell!*/
        
        //PEDIDOS MANUALES
        let cell = tableView.dequeueReusableCell(withIdentifier: "pedidoTVC") as? PedidoTableViewCell
        print("pedidos", setPedidos.count)
        let nombre = setPedidos[indexPath.row].cliente?.nombre
        let apellido = setPedidos[indexPath.row].cliente?.apellido
        if setPedidos[indexPath.row].cliente?.foto != nil{
            let imagen = UIImage(data: (setPedidos[indexPath.row].cliente?.foto)!)
            cell?.clienteImageView.image = imagen
            cell?.clienteImageView.hideActivityIndicator(activityIndicator: cell!.activityIndicator, backgroundView: cell!.fondo)
        } else{
            cell?.clienteImageView.showActivityIndicator(activityIndicator: cell!.activityIndicator, backgroundView: cell!.fondo)
        }
        cell?.nombreClienteLabel.text = "\(nombre!) \(apellido!)"
        cell?.horaPedidoLabel.text = "Pedido para las 12:00pm de hoy"
        switch setPedidos[indexPath.row].estado {
        //Procesado
        case 1:
            cell?.relojProgressView.progress = 1
            cell?.edProgressView.progress = 0
            cell?.relojEstadoImageView.tintColor = UIColor(named: "Accent1")
            cell?.edEstadoImageView.tintColor = UIColor(named: "Accent3")
            cell?.completadoImageView.tintColor = UIColor(named: "Accent3")
        //Procesado y entregado al domiciliario
        case 2:
            cell?.relojProgressView.progress = 1
            cell?.edProgressView.progress = 0.2
            cell?.relojEstadoImageView.tintColor = UIColor(named: "Accent1")
            cell?.edEstadoImageView.tintColor = UIColor(named: "Accent2")
            cell?.completadoImageView.tintColor = UIColor(named: "Accent3")
        //En camino al cliente
        case 3:
            cell?.relojProgressView.progress = 1
            cell?.edProgressView.progress = 0.5
            cell?.relojEstadoImageView.tintColor = UIColor(named: "Accent1")
            cell?.edEstadoImageView.tintColor = UIColor(named: "Accent2")
            cell?.completadoImageView.tintColor = UIColor(named: "Accent2")
        default:
            cell?.relojProgressView.progress = 1
            cell?.edProgressView.progress = 1
            cell?.relojEstadoImageView.tintColor = UIColor(named: "Accent1")
            cell?.edEstadoImageView.tintColor = UIColor(named: "Accent1")
            cell?.completadoImageView.tintColor = UIColor(named: "Accent1")
        }
        return cell!
        /*var a = setPedidos[0].allObjects as? Pedido
        
        cell?.nombreClienteLabel.text = setPedidos[indexPath.row]*/
    }
}

//MARK: - PedidosTableView Delegate
extension ViewControllerPedidos: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pedidoSeleccionado = setPedidos[indexPath.row]
        performSegue(withIdentifier: "VCPedidoDetail", sender: self)
    }
    
    
}
