//
//  CallViewController.swift
//  movsapp
//
//  Created by David Bautista and Juan Daniel Ahumada on 4/04/20.
//  Copyright © 2020 David Bautista. All rights reserved.
//

import UIKit
import AVFoundation
import Speech
import CoreData

class ViewControllerLlamadas: UIViewController, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    //Outlets
    @IBOutlet weak var callTime: UILabel!
    @IBOutlet weak var callState: UILabel!
    @IBOutlet weak var colgarBtn: UIButton!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var textSpeech: UITextView!
    @IBOutlet weak var clientesPicker: UIPickerView!
   
    
    //CODIGO JUAN NUEVO
    let usuario = UserDefaults.standard.string(forKey: "usuario")!
    //Variables
    var voiceRecorder: AVAudioRecorder!
    var recordingSession: AVAudioSession!
    let speechRecognizer        = SFSpeechRecognizer(locale: Locale(identifier: "es-CO"))
    var recognitionRequest      : SFSpeechAudioBufferRecognitionRequest?
    var recognitionTask         : SFSpeechRecognitionTask?
    let audioEngine             = AVAudioEngine()
    var audioPlayer: AVAudioPlayer!
    var fileName : String = "aud";
    var timer = Timer()
    var secs = 0;
    var llamando = false;
    var dest = ""
    var nombre = ""
    var textoPedido = ""
    var items = [String]()
    var itemsCantidad = [String:Int]()
    
    //Clientes DB
    var clientes = [NSManagedObject]()
    var telefonoCliente = 0
    
    //Domiciliarios DB
    var setDomiciliarios = [Domiciliario]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkPermissions()
        textSpeech.isEditable = false
        clientesPicker.dataSource = self
        clientesPicker.delegate = self
        clientesPicker.setValue(UIColor.black, forKeyPath: "textColor")
        let tutorialCompletado = UserDefaults.standard.bool(forKey: "tutorialCompletado")
        if tutorialCompletado == false{
            UserDefaults.standard.set(4, forKey:"tutoCount")
            let tutorial = UIAlertController(title: "Tutorial",
            message: "Acá puedes agregar un nuevo pedido. Por favor presiona el botón azúl para iniciar la llamada, realiza un pedido, selecciona un cliente y presiona finalizar llamada. Solo puedes pedir guayaba, durazno, aguacate, manzana, pera, mango, papa y coco.",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                       style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            tutorial.addAction(ok)
            //Muestra la alerta
            present(tutorial,
            animated: true,
            completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Consulta Clientes
        tabBarItem.title = "Back"
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest : NSFetchRequest<Cliente> = Cliente.fetchRequest()
        do {
            let results = try managedContext.fetch(fetchRequest)
            clientes = results as [NSManagedObject]
        } catch let error as NSError {
            print("No ha sido posible cargar \(error), \(error.userInfo)")
        }
        //Consulta Domiciliarios
        let fetchRequest2 = NSFetchRequest<NSFetchRequestResult>(entityName: "Domiciliario")
        let result2 = try? managedContext.fetch(fetchRequest2)
        let resultData2 = result2 as! [Domiciliario]
        for object2 in resultData2{
            setDomiciliarios.append(object2)
        }
    }
    
    private func handlePermissionFailed() {
        // Present an alert asking the user to change their settings.
        let ac = UIAlertController(title: "This app must have access to speech recognition to work.",
                                   message: "Please consider updating your settings.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Open settings", style: .default) { _ in
            let url = URL(string: UIApplication.openSettingsURLString)!
            UIApplication.shared.open(url)
        })
        ac.addAction(UIAlertAction(title: "Close", style: .cancel))
        present(ac, animated: true)
        
        
        // Disable the record button.
        btnStart.isEnabled = false
        btnStart.setTitle("Speech recognition not available.", for: .normal)
    }
    
    private func checkPermissions() {
        SFSpeechRecognizer.requestAuthorization { authStatus in
            DispatchQueue.main.async {
                switch authStatus {
                case .authorized: break
                default: self.handlePermissionFailed()
                }
            }
        }
    }
    
    func setupRecorder(){
        
        let recordSettings = [AVFormatIDKey : kAudioFormatAppleLossless, AVEncoderAudioQualityKey : AVAudioQuality.max.rawValue, AVEncoderBitRateKey : 320000, AVNumberOfChannelsKey: 2, AVSampleRateKey: 44100.0] as [String : Any]
        
        do {
            voiceRecorder = try AVAudioRecorder(url: getFileURL(), settings: recordSettings)
            voiceRecorder.delegate = self
            voiceRecorder.prepareToRecord()
        }
        catch {
            print("error setting up: \(error)")
        }
    }
    
    
    func startRecording() {

        // Clear all previous session data and cancel task
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        // Create instance of audio session to record voice
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.measurement, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
            self.setupRecorder()
            self.voiceRecorder.record()

        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        self.recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        let inputNode = audioEngine.inputNode
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        if #available(iOS 13, *) {
            if speechRecognizer?.supportsOnDeviceRecognition ?? false{
                recognitionRequest.requiresOnDeviceRecognition = true
            }
        }
        recognitionRequest.shouldReportPartialResults = true
        self.recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            var isFinal = false
            if result != nil {
                self.textSpeech.text = result?.bestTranscription.formattedString
                isFinal = (result?.isFinal)!
            }

            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                self.recognitionRequest = nil
                self.recognitionTask = nil
                self.btnStart.isEnabled = true
            }
        })

        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }

        self.audioEngine.prepare()
        do {
            try self.audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        self.textSpeech.text = "Estoy escuchando, dime tu pedido!"
    }

    @objc func changeCallTimer(){
           if llamando == true {
               secs = secs + 1
               let mins = (secs/60)
               callTime.text = "\(String(format: "%02d", mins)):\(String(format: "%02d", secs%60))"
           }
       }
    
    
    
    
    
    @IBAction func iniciarLlamadaAction(_ sender: Any) {
        if audioEngine.isRunning {
            let yaGrabando = UIAlertController(title: "Grabando",
                                            message: "Ya estás en una llamada.",
                                            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                   style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            yaGrabando.addAction(ok)
            //Muestra la alerta
            present(yaGrabando,
                    animated: true,
                    completion: nil)
        } else {
            llamando = true;
            self.startRecording()
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.changeCallTimer), userInfo: nil, repeats: true)
            let llamadaIniciada = UIAlertController(title: "En llamada",
                                            message: "Ha iniciado tu llamada y se está grabando para el pedido.",
                                            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                   style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            llamadaIniciada.addAction(ok)
            //Muestra la alerta
            present(llamadaIniciada,
                    animated: true,
                    completion: nil)
        }
    }
    
    
    
    func saveAudio() {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        let formattedDate = format.string(from: date)
        
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let destination = documentsDirectoryURL.appendingPathComponent("audio_\(formattedDate)").appendingPathExtension("m4a")
        dest = destination.absoluteString
        nombre = "audio_\(formattedDate)"
        print("saving file \(getFileURL()) in \(destination)")
        do {
            try FileManager.default.moveItem(at: getFileURL(), to: destination)
        } catch {
            print("error retreving audio: \(error)")
        }
    }
    
    func getCacheDirectory() -> URL {
        let fm = FileManager.default
        let docsurl = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        return docsurl
    }
    
    func getFileURL() -> URL {
        let path = getCacheDirectory()
        let filePath = path.appendingPathComponent(fileName).appendingPathExtension("m4a")
        return filePath
    }
    
    func getFileToPlay() -> URL {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as URL
        let soundURL = documentDirectory.appendingPathComponent(fileName)
        return soundURL
    }
    
    
    @IBAction func reproducirGrabacionAction(_ sender: Any) {
        if dest != ""{
            preparePlayer()
            audioPlayer.play()
        }else{
            let llamadaObligatoria = UIAlertController(title: "Error",
                                            message: "Para reproducir primero tienes que grabar una llamada pulsando el botón del teléfono y posteriormente el botón terminar llamada.",
                                            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                   style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            llamadaObligatoria.addAction(ok)
            //Muestra la alerta
            present(llamadaObligatoria,
                    animated: true,
                    completion: nil)
        }
        
    }
    
    func startAudioSession() {
        
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(AVAudioSession.Category.playAndRecord)
            try recordingSession.setActive(true); recordingSession.requestRecordPermission(){ [unowned self] allowed in DispatchQueue.main.async {
                if allowed {
                    self.setupRecorder()
                    self.voiceRecorder.record()
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.changeCallTimer), userInfo: nil, repeats: true)
                    self.llamando = true
                } else {
                    print("sin permisos")
                }
                }
            }
        }catch {
            print("error grabando \(error)")
        }
        
    }
    
    func preparePlayer(){
        do{
            let url = URL(fileURLWithPath: dest)
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                audioPlayer.delegate = self
                audioPlayer.volume = 1.0
            
            
        }catch{
            if let err = error as Error? {
                print ("AVAudioPlayer error: \(err.localizedDescription)")
                audioPlayer = nil
            }
        }
    }
    
    
    @IBAction func terminarLlamadaAction(_ sender: Any) {
        if voiceRecorder != nil{
            analizarTexto()
            if items.count==0{
                textSpeech.text = ""
                voiceRecorder.stop()
                secs = 0
                timer.invalidate()
                timer = Timer()
                llamando = false;
                callTime.text = "00:00"
                self.audioEngine.stop()
                self.recognitionRequest?.endAudio()
                let llamadaFinalizada = UIAlertController(title: "Llamada erronea",
                                                message: "Debes hacer una llamada correcta con los productos disponibles.",
                                                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                       style: .default) { (action: UIAlertAction) -> Void in
                }
                //Agrega el boton de volver a la alerta
                llamadaFinalizada.addAction(ok)
                //Muestra la alerta
                present(llamadaFinalizada,
                        animated: true,
                        completion: nil)
            }else{
                print (textoPedido)
                voiceRecorder.stop()
                self.saveAudio()
                llamando = false;
                secs = 0
                timer.invalidate()
                timer = Timer()
                self.audioEngine.stop()
                self.recognitionRequest?.endAudio()
                let llamadaFinalizada = UIAlertController(title: "Llamada finalizada",
                                                message: "Tu llamada ha finalizado y se está procesando el pedido.",
                                                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                       style: .default) { (action: UIAlertAction) -> Void in
                                        self.navigationController?.popViewController(animated: true)
                }
                //Agrega el boton de volver a la alerta
                llamadaFinalizada.addAction(ok)
                //Muestra la alerta
                present(llamadaFinalizada,
                        animated: true,
                        completion: nil)
                crearPedido()
            }
            
        }else{
            let llamadaObligatoria = UIAlertController(title: "Error",
                                            message: "Debes iniciar una llamada válida para poder hacer el pedido presionando el botón del teléfono.",
                                            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                   style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            llamadaObligatoria.addAction(ok)
            //Muestra la alerta
            present(llamadaObligatoria,
                    animated: true,
                    completion: nil)
        }
    }
    
    
    
    func analizarTexto(){
        let texto = textSpeech.text!
        print(texto)
        let text = texto.lowercased()
        let palabras = text.components(separatedBy: " ")
        var cantidad = [Int]()
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "es_CO")
        numberFormatter.numberStyle = .spellOut
        for var p in palabras{
            p = p.stripped
            if let number = Int(p) {
                cantidad.append(number)
            }
            
            if let num = numberFormatter.number(from: p)?.stringValue{
                if let number = Int(num) {
                    cantidad.append(number)
                }
            }
            if (p == "aguacate" || p == "aguacates"){
                items.append("Aguacate")
            }else if (p == "coco" || p == "cocos"){
                items.append("Coco")
            }else if (p == "durazno" || p == "duraznos"){
                items.append("Durazno")
            }else if (p == "guayaba" || p == "guayabas"){
                items.append("Guayaba")
            }else if (p == "mango" || p == "mangos"){
                items.append("Mango")
            }else if (p == "manzana" || p == "manzanas"){
                items.append("Manzana")
            }else if (p == "papa" || p == "papas"){
                items.append("Papa")
            }else if (p == "pera" || p == "peras"){
                items.append("Pera")
            }
        }
        
        var i = items.count-1
        
        if (items.count == cantidad.count){
            while (i>=0){
                itemsCantidad[items[i]] = cantidad[i]
                i -= 1
            }
        }else{
            items = [String]()
            itemsCantidad = [String:Int]()
        }
    }
    
    // id: Telefono cliente, itemsCarrito [String], itemsCantidad [String:Int], domi: setDomiciliarios[0]
    func crearPedido(){
        let rand = arc4random_uniform(UInt32(setDomiciliarios.count))
        let domi = setDomiciliarios[Int(rand)]
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
        let formato = "telefono = \(telefonoCliente)"
        fetchRequest.predicate = NSPredicate(format: formato)
        let result = try? managedObjectContext.fetch(fetchRequest)
        let resultData = result as! [Cliente]
        let pedido = Pedido(context: managedObjectContext)
        pedido.estado = 1
        pedido.fechaCreacion = Date()
        pedido.urlAudio = dest
        pedido.vozATexto = textSpeech.text
        pedido.guardadoRemoto = false
        pedido.usuario = usuario
        //pedido.fechaCreacion = Date(timeIntervalSinceNow: -345612)
        pedido.guardadoRemoto = false
        domi.addToPedidos(pedido)
        for it in items{
            let item = Item(context: managedObjectContext)
            let cant = itemsCantidad[it]
            item.nombre = it
            item.cantidad = Int64(cant!)
            pedido.addToItems(item)
        }
        for object in resultData{
            object.addToPedidos(pedido)
        }
        
        //Agregar a firebase
        do {
            try managedObjectContext.save()
            print("saved!")
            subirPedidos(usuario: usuario, context: managedObjectContext)
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
    }
    
}


// MARK: -ClientesPickerView DataSource Delegate
extension ViewControllerLlamadas: UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var rows = 0
        if pickerView == self.clientesPicker{
            if clientes.count == 0{
                rows = 1
            }else{
                rows = clientes.count
                telefonoCliente = clientes[0].value(forKey: "telefono") as! Int
            }
        }
        
        return rows
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var lineText = " "
        if clientes.count == 0{
            lineText = "No hay clientes registrados"
        }else{
            let cliente = clientes[row]
            let nombre = cliente.value(forKey: "nombre") as? String
            let apellido = cliente.value(forKey: "apellido") as? String
            lineText = "\(nombre!) \(apellido!)"
            }
        return lineText
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        telefonoCliente = clientes[row].value(forKey: "telefono") as! Int
    }
}


extension String {

    var stripped: String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-=().!_")
        return self.filter {okayChars.contains($0) }
    }
}
    //Código DAVID FUNCIONANDO SIN MODIFICACIONES
    /*
    
    
    
    var voiceRecorder: AVAudioRecorder!
    var recordingSession: AVAudioSession!
    var fileName : String = "aud";
    var timer: Timer!
    var secs = 0;
    var llamando = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startAudioSession()
    }
    
    @objc func changeCallTimer(){
        if llamando == true {
            secs = secs + 1
            let mins = (secs/60)
            callTime.text = "\(String(format: "%02d", mins)):\(String(format: "%02d", secs%60))"
        }
    }
    
    func startAudioSession() {
        
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(AVAudioSession.Category.playAndRecord)
            try recordingSession.setActive(true); recordingSession.requestRecordPermission(){ [unowned self] allowed in DispatchQueue.main.async {
                if allowed {
                    self.setupRecorder()
                    self.voiceRecorder.record()
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.changeCallTimer), userInfo: nil, repeats: true)
                    self.llamando = true
                } else {
                    print("sin permisos")
                }
                }
            }
        }catch {
            print("error grabando \(error)")
        }
        
    }
    
    
    
    
    func setupRecorder(){
        let recordSettings = [AVFormatIDKey : kAudioFormatAppleLossless, AVEncoderAudioQualityKey : AVAudioQuality.max.rawValue, AVEncoderBitRateKey : 320000, AVNumberOfChannelsKey: 2, AVSampleRateKey: 44100.0] as [String : Any]
        
        do {
            voiceRecorder = try AVAudioRecorder(url: getFileURL(), settings: recordSettings)
            voiceRecorder.delegate = self
            voiceRecorder.prepareToRecord()
        }
        catch {
            print("error setting up: \(error)")
        }
    }
    
    func saveAudio() {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        let formattedDate = format.string(from: date)
        
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let destination = documentsDirectoryURL.appendingPathComponent("audio_\(formattedDate)").appendingPathExtension("m4a")
        print("saving file \(getFileURL()) in \(destination)")
        do {
            try FileManager.default.moveItem(at: getFileURL(), to: destination)
        } catch {
            print("error retreving audio: \(error)")
        }
    }
    
    func getCacheDirectory() -> URL {
        let fm = FileManager.default
        let docsurl = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        return docsurl
    }
    
    func getFileURL() -> URL {
        
        
        let path = getCacheDirectory()
        let filePath = path.appendingPathComponent(fileName).appendingPathExtension("m4a")
        return filePath
    }
    
    
    @IBAction func colgar(sender: UIButton) {
        if llamando == true {
            voiceRecorder.stop()
            self.saveAudio()
            colgarBtn.isEnabled = false
            llamando = false;
        }
        let pedidoExitoso = UIAlertController(title: "Pedido creado",
        message: "El pedido ha sido guardado.",
        preferredStyle: .alert)
        
        //Creamos el UIAlertAction que nos permitirá volver
        let volver = UIAlertAction(title: "Ok",
                                   style: .default) { (action: UIAlertAction) -> Void in
                                    //self.navigationController?.popViewController(animated: true)
        }
        //Agrega el boton de volver a la alerta
        pedidoExitoso.addAction(volver)
        //Muestra la alerta
        present(pedidoExitoso,
        animated: true,
        completion: nil)
        
        
        
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        llamando = false;
        colgarBtn.isEnabled = false
    }
    
}*/

