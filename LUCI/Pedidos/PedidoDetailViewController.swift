//
//  PedidoDetailViewController.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 16/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit
import AVFoundation

class PedidoDetailViewController: UIViewController, AVAudioPlayerDelegate {
    // Outlets
    @IBOutlet weak var audioBtn: UIButton!
    @IBOutlet weak var clienteImageView: UIImageView!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var horaPedidoLabel: UILabel!
    @IBOutlet weak var listaCompraTableView: UITableView!
    @IBOutlet weak var procesadoProgressView: UIProgressView!
    @IBOutlet weak var entregadoProgressView: UIProgressView!
    
    @IBOutlet weak var empacadoProgressView: UIProgressView!
    @IBOutlet weak var empacarButton: UIButton!
    
    //Variable
    var pedido: Pedido?
    var items = [Item]()
    //Para reproducir el audio
    var audioPlayer: AVAudioPlayer!
    var session: AVAudioSession!
    
    //Items imágenes
    let itemsImages: [String: UIImage?] = ["Aguacate":UIImage(named: "aguacate"),"Manzana":UIImage(named: "manzana"),"Pera":UIImage(named: "pera"),"Mango":UIImage(named: "mango"),"Papa":UIImage(named: "papa"),"Guayaba":UIImage(named: "guayaba"),"Coco":UIImage(named: "coco"),"Durazno":UIImage(named: "durazno")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //Cliente ImageView
        clienteImageView.image = UIImage(data: (pedido!.cliente?.foto)!)
        //Label NombreCliente
        nombreLabel.text = pedido!.cliente?.nombre
        if pedido!.urlAudio == nil{
            audioBtn.isHidden = true
            audioBtn.isEnabled = false
        }
        
        //Label HoraPedido
        let format = DateFormatter()
        format.dateFormat = "yy/MM/dd HH:mm"
        let formattedDate = format.string(from: (pedido?.fechaCreacion)!)
        horaPedidoLabel.text = formattedDate
        //CarritoTableView
        listaCompraTableView.dataSource = self
        listaCompraTableView.tableFooterView = UIView()
        listaCompraTableView.register(UINib(nibName: "CarritoTableViewCell", bundle: nil), forCellReuseIdentifier: "carritoTVC")
        // Do any additional setup after loading the view.
        items = pedido!.items?.allObjects as! [Item]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        switch pedido!.estado {
        case 1:
            procesadoProgressView.progress = 1
            empacadoProgressView.progress = 0
            entregadoProgressView.progress = 0
        case 2:
            procesadoProgressView.progress = 1
            empacadoProgressView.progress = 1
            entregadoProgressView.progress = 0
            empacarButton.setTitle("Enviar pedido", for: .normal)
        case 3:
            procesadoProgressView.progress = 1
            empacadoProgressView.progress = 1
            entregadoProgressView.progress = 0.5
            empacarButton.setTitle("El domiciliario ha llegado", for: .normal)
        default:
            procesadoProgressView.progress = 1
            empacadoProgressView.progress = 1
            entregadoProgressView.progress = 1
            empacarButton.isHidden = true
            empacarButton.isEnabled = false
        }
        let tutorialCompletado = UserDefaults.standard.bool(forKey: "tutorialCompletado")
        let estadoTuto = UserDefaults.standard.integer(forKey: "tutoCount")
        if tutorialCompletado == false{
            if estadoTuto == 4{
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Acá ves los detalles del pedido, presiona el botón enviar pedido para poder continuar con el pedido.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            } else if estadoTuto == 10{
                UserDefaults.standard.set(true, forKey: "tutorialCompletado")
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Felicidades!! Has terminado el tutorial con éxito, ahora puedes usar LUCI!.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                                            self.navigationController?.popViewController(animated: true)
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            }
            else{
                UserDefaults.standard.set(8, forKey:"tutoCount")
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Presiona el el domiciliario ha llegado para volver a la pantalla de control del estado.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            }
            
        }
    }
    
    @IBAction func pedidoEmpacadoAction(_ sender: Any) {
        performSegue(withIdentifier: "VCPedidoADomiciliario", sender: self)
        
    }
    @IBAction func escucharAudioPedidoAction(_ sender: Any) {
        startAudioSession()
        preparePlayer()
        audioPlayer.play()
    }
    
    func startAudioSession() {
        
        session = AVAudioSession.sharedInstance()
        
        do {
            try session.setCategory(AVAudioSession.Category.playback, mode: .default, options: .defaultToSpeaker);
            try session.setActive(true);
        }catch {
            //failed try
        }
        
    }
    
    func preparePlayer() {
        do {
            let url = URL(fileURLWithPath: (pedido?.urlAudio!)!)
            audioPlayer = try AVAudioPlayer(contentsOf: (url))
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            audioPlayer.volume = 1.0
        } catch {
            print("audio play failed error: \(error)")
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VCPedidoADomiciliario"{
            if let destino = segue.destination as? DomiciliarioDetailViewController{
                destino.domiciliario = pedido?.domiciliario
            }
        }
    }
    
}

//MARK: -ItemsTableView DataSource
extension PedidoDetailViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "carritoTVC") as? CarritoTableViewCell
        let item = items[indexPath.row]
        let nom = item.nombre!
        cell?.nombreLabel.text = nom        
        cell?.itemImageView.image = itemsImages[item.nombre!]!
        cell?.cantidadLabel.text = String(item.cantidad)
        return cell!
    }
}
