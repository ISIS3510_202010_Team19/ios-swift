//
//  ClienteFavCollectionViewCell.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 11/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit

class ClienteFavCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var principalView: UIView!
    @IBOutlet weak var clienteImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    var fondo: UIView = {
      let backgroundView = UIView()
      backgroundView.backgroundColor = UIColor.black
      backgroundView.alpha = 0.8
      backgroundView.layer.cornerRadius = 0
      backgroundView.layer.masksToBounds = true
      
      return backgroundView
    }()
    
    var activityIndicator: UIActivityIndicatorView = {
        var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
      activityIndicator.hidesWhenStopped = true
      activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        activityIndicator.style = UIActivityIndicatorView.Style.large
      activityIndicator.autoresizingMask = [.flexibleLeftMargin , .flexibleRightMargin , .flexibleTopMargin , .flexibleBottomMargin]
      activityIndicator.isUserInteractionEnabled = false
      return activityIndicator
    }()

}
