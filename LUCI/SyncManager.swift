//
//  SyncManager.swift
//  LUCI
//
//  Created by David Bautista on 18/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import Foundation
import Reachability
import CoreData
import SystemConfiguration
import FirebaseFirestore
import FirebaseStorage
import SDWebImage
import AVFoundation
var allPedidos = [Pedido]()



func printSomenthing(context : NSManagedObjectContext){
    

    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Pedido")
//
//    let deleterequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//    do {
//    try context.execute(deleterequest)
//    }catch{
//        print("no se pudo borrar nada ")
//    }
    let result = try? context.fetch(fetchRequest)

    let resultData = result as! [Pedido]
    
    let today = Date()
    let calendar = NSCalendar.current
    
    for (index, object) in resultData.enumerated(){
//        object.guardadoRemoto = true;
//        do { try context.save()}catch {print("falla guardando estado true")}
        
        print("************ pedido \(index)", object.guardadoRemoto)
        print("analizando si borrar el pedido \(index) de la base de datos local")
        guard let pedidoFirebaseState: Bool = object.guardadoRemoto else {
            print("no esta en remoto, guardando")
            DatabaseService.shared.savePedido(pedido: object, index: index, context: context)
            continue
        }
        
        if pedidoFirebaseState == false {
            print("no esta en remoto, guardando")
            DatabaseService.shared.savePedido(pedido: object, index: index, context: context)
            continue
        }
        
        
        guard let pedidoDate = object.fechaCreacion else {
            print("pedido \(index) has no date")
            continue
        }
        guard let days = calendar.dateComponents([.day], from: pedidoDate, to: today).day else {
            continue
        }
        
        
        if days > 2 && pedidoFirebaseState == true {
            print("pedido is old, deleting from local storage")
            context.delete(object)
        }
    }
}

func subirDatosLocales(context : NSManagedObjectContext){
    let usuario = UserDefaults.standard.string(forKey: "usuario")!
    
    subirClientes(usuario: usuario, context:context)
    subirDomiciliarios(usuario: usuario, context: context)
    subirPedidos(usuario: usuario, context: context)
}

func syncClientes(usuario: String, context : NSManagedObjectContext){
    //Se recuperan los clientes remotos
    let db = Firestore.firestore()
    let docRef = db.collection("Usuarios").document(usuario)
    let entity = NSEntityDescription.entity(forEntityName: "Cliente", in: context)
    docRef.collection("Clientes").addSnapshotListener { (querySnapshot, err) in
        if let err = err {
            print("Error getting documents: \(err)")
        }else {
            guard let snapshot = querySnapshot else {
                    print("Error fetching snapshots: \(err!)")
                    return
                }
            snapshot.documentChanges.forEach { diff in
                if (diff.type == .added) {
                    let data = diff.document.data()
                    
                    let telefono = data["telefono"] as! Int64
                    let enLocal = verificarCoreData(id: telefono, context: context, tipo: "Cliente")
                    if !enLocal{
                        let cliente = Cliente(entity: entity!, insertInto: context)
                        let nombre = data["nombre"] as! String
                        let direccion = data["direccion"] as! String
                        
                        let apellido = data["apellido"] as! String
                        let favorito = data["favorito"] as! Bool
                        let fotoURL = data["foto"] as! String
                        let guardadoRemoto = data["guardadoRemoto"] as! Bool
                        cliente.nombre = nombre
                        cliente.direccion = direccion
                        cliente.telefono = telefono
                        cliente.apellido = apellido
                        cliente.favorito = favorito
                        cliente.usuario = usuario
                        cliente.guardadoRemoto = guardadoRemoto
                        descargarImagen(url: fotoURL, object: cliente, context: context, tipo: "Cliente")
                        do {
                            try context.save()
                          } catch let error as NSError {
                          print("No ha sido posible guardar \(error), \(error.userInfo)")
                        }
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newDataNotif"), object: nil)
                    }
                }
                else if (diff.type == .modified){
                    let data = diff.document.data()
                    let nombre = data["nombre"] as! String
                    let direccion = data["direccion"] as! String
                    let apellido = data["apellido"] as! String
                    let favorito = data["favorito"] as! Bool
                    let fotoURL = data["foto"] as! String
                    let telefono = data["telefono"] as! Int
                    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
                    let result = try? context.fetch(fetchRequest)
                    let resultData = result as! [Cliente]
                    for cliente in resultData{
                        if (cliente.usuario == usuario && cliente.telefono == telefono){
                            cliente.setValue(nombre, forKey: "nombre")
                            cliente.setValue(direccion, forKey: "direccion")
                            cliente.setValue(telefono, forKey: "telefono")
                            cliente.setValue(apellido, forKey: "apellido")
                            cliente.setValue(favorito, forKey: "favorito")
                            cliente.setValue(usuario, forKey: "usuario")
                            cliente.setValue(true, forKey: "guardadoRemoto")
                            descargarImagen(url: fotoURL, object: cliente, context: context, tipo: "Cliente")
                        }
                    }
                    do {
                        try context.save()
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newDataNotif"), object: nil)
                }
                else if (diff.type == .removed) {
                    let data = diff.document.data()
                    let telefono = data["telefono"] as! Int
                    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
                    let result = try? context.fetch(fetchRequest)
                    let resultData = result as! [Cliente]
                    for cliente in resultData{
                        if (cliente.usuario == usuario && cliente.telefono == telefono){
                            context.delete(cliente)
                            let pedidos = cliente.pedidos?.allObjects as! [Pedido]
                            for p in pedidos{
                                context.delete(p)
                        }
                        }
                    }
                        
                        
                    do {
                        try context.save()
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newDataNotif"), object: nil)
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newDownloadFinished"), object: nil,userInfo: ["data":"clientesFinalizado"])
        }
    }
}


func syncDomiciliarios(usuario: String, context : NSManagedObjectContext){
    //Se recuperan los clientes remotos
    let db = Firestore.firestore()
    let docRef = db.collection("Usuarios").document(usuario)
    let entity = NSEntityDescription.entity(forEntityName: "Domiciliario", in: context)
    docRef.collection("Domiciliarios").addSnapshotListener{ (querySnapshot, err) in
        if let err = err {
            print("Error getting documents: \(err)")
        } else {
            guard let snapshot = querySnapshot else {
                print("Error fetching snapshots: \(err!)")
                return
            }
            snapshot.documentChanges.forEach { diff in
                if (diff.type == .added) {
                    let data = diff.document.data()
                    let telefono = data["telefono"] as! Int64
                    let enLocal = verificarCoreData(id: telefono, context: context, tipo: "Domiciliario")
                    if !enLocal{
                        let apellido = data["apellido"] as! String
                        let enRuta = data["enRuta"] as! Bool
                        let fotoURL = data["foto"] as! String
                        let guardadoRemoto = data["guardadoRemoto"] as! Bool
                        let nombre = data["nombre"] as! String
                        
                        let usuario = data["usuario"] as! String
                        let domiciliario = Domiciliario(entity: entity!, insertInto: context)
                        domiciliario.apellido = apellido
                        domiciliario.enRuta = enRuta
                        domiciliario.guardadoRemoto = guardadoRemoto
                        domiciliario.nombre = nombre
                        domiciliario.telefono = telefono
                        domiciliario.usuario = usuario
                        descargarImagen(url: fotoURL, object: domiciliario, context: context, tipo: "Domiciliario")
                        do {
                            try context.save()
                          } catch let error as NSError {
                          print("No ha sido posible guardar \(error), \(error.userInfo)")
                        }
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newDataNotif"), object: nil)
                    }
                }
                else if (diff.type == .modified) {
                    let data = diff.document.data()
                    let apellido = data["apellido"] as! String
                    let enRuta = data["enRuta"] as! Bool
                    let fotoURL = data["foto"] as! String
                    let guardadoRemoto = data["guardadoRemoto"] as! Bool
                    let nombre = data["nombre"] as! String
                    let telefono = data["telefono"] as! Int
                    let usuario = data["usuario"] as! String
                    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Domiciliario")
                    let result = try? context.fetch(fetchRequest)
                    let resultData = result as! [Domiciliario]
                    for domiciliario in resultData{
                        if (domiciliario.usuario == usuario && domiciliario.telefono == telefono){
                            domiciliario.apellido = apellido
                            domiciliario.enRuta = enRuta
                            domiciliario.guardadoRemoto = guardadoRemoto
                            domiciliario.nombre = nombre
                            domiciliario.telefono = Int64(telefono)
                            domiciliario.usuario = usuario
                            descargarImagen(url: fotoURL, object: domiciliario, context: context, tipo: "Domiciliario")
                        }
                    }
                    do {
                        try context.save()
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newDataNotif"), object: nil)
                }
                else if (diff.type == .removed) {
                    let data = diff.document.data()
                    let telefono = data["telefono"] as! Int
                    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Domiciliario")
                    let result = try? context.fetch(fetchRequest)
                    let resultData = result as! [Domiciliario]
                    for domiciliario in resultData{
                        if (domiciliario.usuario == usuario && domiciliario.telefono == telefono){
                            context.delete(domiciliario)
                            break
                        }
                    }
                    do {
                        try context.save()
                    } catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newDownloadFinished"), object: nil,userInfo: ["data":"domiciliariosFinalizado"])
        }
    }
}


func syncPedidos(usuario: String, context : NSManagedObjectContext){
    //Se recuperan los clientes remotos
    let db = Firestore.firestore()
    let docRef = db.collection("Usuarios").document(usuario)
    let entity = NSEntityDescription.entity(forEntityName: "Pedido", in: context)
    docRef.collection("Pedidos").addSnapshotListener{ (querySnapshot, err) in
        if let err = err {
            print("Error getting documents: \(err)")
        } else {
            guard let snapshot = querySnapshot else {
                print("Error fetching snapshots: \(err!)")
                return
            }
            snapshot.documentChanges.forEach { diff in
                    if (diff.type == .added) {
                        let data = diff.document.data()
                        let id = diff.document.documentID
                        let enLocal = verificarCoreData(id: id, context: context, tipo: "Pedido")
                        if !enLocal{
                            let estado = data["estado"] as! Int
                            let creationDate = data["fechaCreacion"] as! Timestamp
                            let usuario = data["usuario"] as! String
                            let clienteId = data["cliente"] as! Int64
                            let domiciliarioId = data["domiciliario"] as! Int
                            let fechaCreacion = creationDate.dateValue()
                            
                            
                            
                            let pedido = Pedido(context: context)
                            pedido.estado = Int64(estado)
                            pedido.fechaCreacion = fechaCreacion
                            pedido.guardadoRemoto = true
                            pedido.id = id
                            
                            if let urlAudio = data["urlAudio"] as? String{
                                pedido.urlAudio = urlAudio
                            }
                            if let vozATexto = data["vozATexto"] as? String{
                                pedido.vozATexto = vozATexto
                            }
                            pedido.usuario = usuario
                            
                            let fetchRequestCliente = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
                            let resultCliente = try? context.fetch(fetchRequestCliente)
                            let resultDataCliente = resultCliente as! [Cliente]
                            for cliente in resultDataCliente{
                                if (cliente.telefono == clienteId){
                                    pedido.cliente = cliente
                                    cliente.addToPedidos(pedido)
                                    break
                                }
                            }
                            
                            let fetchRequestDomiciliario = NSFetchRequest<NSFetchRequestResult>(entityName: "Domiciliario")
                            let resultDomiciliario = try? context.fetch(fetchRequestDomiciliario)
                            let resultDataDomiciliario = resultDomiciliario as! [Domiciliario]
                            for domiciliario in resultDataDomiciliario{
                                if domiciliario.telefono == domiciliarioId{
                                    pedido.domiciliario = domiciliario
                                    domiciliario.addToPedidos(pedido)
                                    break
                                }
                            }
                            docRef.collection("Pedidos").document(id).collection("Items").getDocuments() { (querySnapshot, err) in
                                if let err = err {
                                    print("Error getting documents: \(err)")
                                } else {
                                    for document in querySnapshot!.documents {
                                        let item = Item(context: context)
                                        let itemInfo = document.data()
                                        let nombre = itemInfo["nombre"] as! String
                                        let cantidad = itemInfo["cantidad"] as! Int64
                                        item.nombre = nombre
                                        item.cantidad = cantidad
                                        pedido.addToItems(item)
                                    }
                                }
                            }
                            do {
                                    try context.save()
                                  } catch let error as NSError {
                                  print("No ha sido posible guardar \(error), \(error.userInfo)")
                                }
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newDataNotif"), object: nil)
                        }
                    }
                else if (diff.type == .modified) {
                    let data = diff.document.data()
                    let id = diff.document.documentID
                    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Pedido")
                    let result = try? context.fetch(fetchRequest)
                    let resultData = result as! [Pedido]
                    for pedido in resultData{
                        if (pedido.usuario == usuario && pedido.id == id){
                            let estado = data["estado"] as! Int64
                            pedido.estado = estado
                            }
                            do {
                                    try context.save()
                                  } catch let error as NSError {
                                  print("No ha sido posible guardar \(error), \(error.userInfo)")
                                }
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newDataNotif"), object: nil)
                        }
                    }
                
                
                    else if (diff.type == .removed) {
                        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Pedido")
                        let result = try? context.fetch(fetchRequest)
                        let resultData = result as! [Pedido]
                        for pedido in resultData{
                            if (pedido.usuario == usuario && pedido.guardadoRemoto == true){
                                context.delete(pedido)
                            }
                        }
                        do {
                            try context.save()
                        } catch let error as NSError  {
                            print("Could not save \(error), \(error.userInfo)")
                        }
                    }
                }
            }
            
    }
    }


func verificarCoreData(id: Any, context: NSManagedObjectContext, tipo: String)->Bool{
    var esta = false
    if type(of: id) == Int64.self{
        let idCliente = id as! Int64
        if tipo == "Cliente"{
            let fetchRequestCliente = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
            let resultCliente = try? context.fetch(fetchRequestCliente)
            let resultDataCliente = resultCliente as! [Cliente]
            for cliente in resultDataCliente{
                if (cliente.telefono == idCliente){
                    esta = true
                    break
                }
            }
        }
        else if tipo == "Domiciliario"{
            let idDomiciliario = id as! Int64
            let fetchRequestCliente = NSFetchRequest<NSFetchRequestResult>(entityName: "Domiciliario")
            let resultCliente = try? context.fetch(fetchRequestCliente)
            let resultDataCliente = resultCliente as! [Domiciliario]
            for domiciliario in resultDataCliente{
                if (domiciliario.telefono == idDomiciliario){
                    esta = true
                    break
                }
            }
        }
    }else{
        let idPedido = id as! String
        let fetchRequestCliente = NSFetchRequest<NSFetchRequestResult>(entityName: "Pedido")
        let resultCliente = try? context.fetch(fetchRequestCliente)
        let resultDataCliente = resultCliente as! [Pedido]
        for pedido in resultDataCliente{
            
                print (pedido.id!)
                if pedido.id == idPedido{
                    esta=true
                    print("entra")
                }
            
        }
    }
    
    return esta
}


func subirClientes(usuario: String, context : NSManagedObjectContext){
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
    let result = try? context.fetch(fetchRequest)
    let resultData = result as! [Cliente]
    for cliente in resultData{
        if (cliente.usuario == usuario && cliente.guardadoRemoto == false){
                let db = Firestore.firestore()
                let docRef = db.collection("Usuarios").document(usuario)
                let ref = docRef.collection("Clientes").document(String(cliente.telefono))
                ref.getDocument { (document, error) in
                    if let document = document, document.exists {
                        print("El cliente ya estaba en la base de datos. Actualizando atributo local.....")
                    } else {
                        let fotoCliente = createImageThumbnail(UIImage(data: cliente.foto!)!)
                        let nombreImagen = cliente.usuario!+String(cliente.telefono)+".jpeg"
                        ref.setData([
                            "nombre": cliente.nombre!,
                            "apellido": cliente.apellido!,
                            "direccion": cliente.direccion!,
                            "telefono": cliente.telefono ,
                            "favorito": cliente.favorito ,
                            "guardadoRemoto": false,
                            "foto": ""
                        ]) { err in
                            if let err = err {
                                print("Error writing document: \(err)")
                            } else {
                                print("Se subió exitosamente el cliente \(cliente.nombre ?? "name") con teléfono \(cliente.telefono ) a la base de datos remota")
                                subirImagen(foto: fotoCliente, path: "clientes/"+nombreImagen, dataRef: ref,object:cliente, context: context)
                            }
                        }
                    }
                }
        }
    }
}

func subirDomiciliarios(usuario: String, context : NSManagedObjectContext){
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Domiciliario")
    let result = try? context.fetch(fetchRequest)
    let resultData = result as! [Domiciliario]
    for domiciliario in resultData{
        if (domiciliario.usuario == usuario && domiciliario.guardadoRemoto == false){
                let db = Firestore.firestore()
                let docRef = db.collection("Usuarios").document(usuario)
                let ref = docRef.collection("Domiciliarios").document(String(domiciliario.telefono))
                ref.getDocument { (document, error) in
                    if let document = document, document.exists {
                        print("El cliente ya estaba en la base de datos. Actualizando atributo local.....")
                    } else {
                        let fotoDomiciliario = createImageThumbnail(UIImage(data: domiciliario.foto!)!)
                        let nombreImagen = domiciliario.usuario!+String(domiciliario.telefono)+".jpeg"
                        ref.setData([
                            "apellido": domiciliario.apellido!,
                            "enRuta": domiciliario.enRuta,
                            "nombre": domiciliario.nombre!,
                            "telefono": domiciliario.telefono,
                            "guardadoRemoto": false,
                            "usuario": domiciliario.usuario!,
                            "foto": ""
                        ]) { err in
                            if let err = err {
                                print("Error writing document: \(err)")
                            } else {
                                print("Se subió exitosamente el domiciliario \(domiciliario.nombre ?? "nombre") con teléfono \(domiciliario.telefono) a la base de datos remota")
                                subirImagen(foto: fotoDomiciliario, path: "domiciliarios/"+nombreImagen, dataRef: ref,object:domiciliario, context: context)
                            }
                        }
                    }
                }
                    do {
                    try context.save()
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                }
        }
    }
}

func subirPedidos(usuario: String, context : NSManagedObjectContext){
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Pedido")
    let result = try? context.fetch(fetchRequest)
    let resultData = result as! [Pedido]
    for pedido in resultData{
        if (pedido.usuario == usuario && pedido.guardadoRemoto == false){
            let db = Firestore.firestore()
            var ref: DocumentReference? = nil
            let docRef = db.collection("Usuarios").document(usuario)
            var vozATexto = ""
            if let vozT = pedido.vozATexto {
                vozATexto = vozT
            }
            
            ref = docRef.collection("Pedidos").addDocument(data: [
                "estado": pedido.estado,
                "fechaCreacion": pedido.fechaCreacion!,
                "guardadoRemoto": true,
                "urlAudio": "",
                "usuario": pedido.usuario!,
                "vozATexto": vozATexto,
                "cliente": pedido.cliente?.telefono ?? 0,
                "domiciliario": pedido.domiciliario?.telefono ?? 0
            ]) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    print("Document added with ID: \(ref!.documentID)")
                }
            }
            pedido.id = ref!.documentID
            pedido.guardadoRemoto = true
            do {
                try context.save()
            } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
            }
            for item in pedido.items!.allObjects{
                let it = item as! Item
                let docR = db.collection("Usuarios").document(usuario).collection("Pedidos").document(ref!.documentID).collection("Items").addDocument(data: ["cantidad":it.cantidad,"nombre":it.nombre!,"pedido":ref?.documentID]) {
                    
                    err in
                    if let err = err{
                        print ("Error adding document: \(err)")
                    }else{
                        print("Document added")
                    }
                }
            }
            if let audio = pedido.urlAudio{
                let url = URL(fileURLWithPath: audio)
                let nombreArchivo = ref!.documentID+".m4a"
                //subirAudio(audio: url.dataRepresentation, path: "pedidos/"+nombreArchivo, dataRef: docRef.collection("Pedidos").document(ref!.documentID), object: pedido, context: context)
            }
        }
    }
}

func subirAudio(audio: Data, path:String, dataRef: DocumentReference, object: Any, context: NSManagedObjectContext){
    let audioRef = Storage.storage().reference().child(path)
    let metadata  = StorageMetadata()
    metadata.contentType = "audio/m4a"
    audioRef.putData(audio, metadata: metadata) { (metadata, error) in
        if let error = error {
            print(error.localizedDescription)
            return
        }
        audioRef.downloadURL(completion: { (url, error) in
        if let error = error {
            print(error.localizedDescription)
            return
        }
        guard let url = url else{
            print("error")
            return
            }
            dataRef.updateData(["urlAudio":url.absoluteString,"guardadoRemoto":true]){ err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Document successfully updated")
                    let pedido = object as! Pedido
                    pedido.guardadoRemoto = true
                    do{
                        try context.save()
                    }catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                }
            }
        })
    }
}

func syncDatosConUsuario(correo: String,context : NSManagedObjectContext){
    let fetchRequestP = NSFetchRequest<NSFetchRequestResult>(entityName: "Pedido")
    let resultP = try? context.fetch(fetchRequestP)
    let resultDataP = resultP as! [Pedido]
    for pedido in resultDataP{
        if pedido.usuario != correo{
            context.delete(pedido)
        }
    }
    
    let fetchRequestC = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
    let resultC = try? context.fetch(fetchRequestC)
    let resultDataC = resultC as! [Cliente]
    for cliente in resultDataC{
        if cliente.usuario != correo{
            context.delete(cliente)
        }
    }
    
    let fetchRequestD = NSFetchRequest<NSFetchRequestResult>(entityName: "Domiciliario")
    let resultD = try? context.fetch(fetchRequestD)
    let resultDataD = resultD as! [Domiciliario]
    for domiciliario in resultDataD{
        if domiciliario.usuario != correo{
            context.delete(domiciliario)
        }
    }
    do{
        try context.save()
    }catch let error as NSError  {
        print("Could not save \(error), \(error.userInfo)")
    }
}

func subirImagen(foto: Data, path:String, dataRef: DocumentReference, object: Any, context: NSManagedObjectContext){
    let imageRef = Storage.storage().reference().child(path)
    imageRef.putData(foto, metadata: nil) { (metadata, error) in
        if let error = error {
            print(error.localizedDescription)
            return
        }
        imageRef.downloadURL(completion: { (url, error) in
        if let error = error {
            print(error.localizedDescription)
            return
        }
        guard let url = url else{
            print("error")
            return
            }
            
            dataRef.updateData(["foto":url.absoluteString,"guardadoRemoto":true]){ err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Document successfully updated")
                    if  type(of: object) == Cliente.self{
                        let cliente = object as! Cliente
                        cliente.guardadoRemoto = true
                    }else{
                        let domiciliario = object as! Domiciliario
                        domiciliario.guardadoRemoto = true
                    }
                    do{
                        try context.save()
                    }catch let error as NSError  {
                        print("Could not save \(error), \(error.userInfo)")
                    }
                }
            }
        })
    }
}

func descargarImagen(url: String, object: NSManagedObject, context: NSManagedObjectContext, tipo: String){
    guard let downloadURL = URL(string: url) else {return }
    let imageManager = SDWebImageManager()
    imageManager.loadImage(with: downloadURL, options: [], progress: {
        (receivedSize, totalSize, url) in
        let percentage = (Float(receivedSize) / Float(totalSize)) * 100.0
        print("downloading progress: \(percentage)%")
    }) {(image,data,err,cacheType,finished,url) in
        if finished {
            print("Imagen descargada")
            if tipo=="Cliente"{
                let cliente = object as! Cliente
                cliente.setValue(image!.pngData()!, forKey: "foto")
            }else{
                let domiciliario = object as! Domiciliario
                domiciliario.setValue(image?.pngData(), forKey: "foto")
            }
            do{
                try context.save()
            }catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newDataNotif"), object: nil)
        }
        else{
            print("No se pudo descargar la imagen")
        }
    }
}



  
  enum ReachabilityStatus {
    case notReachable
    case reachableViaWWAN
    case reachableViaWiFi
  }
  
  var currentReachabilityStatus: ReachabilityStatus {
    
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
      $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
        SCNetworkReachabilityCreateWithAddress(nil, $0)
      }
    }) else {
      return .notReachable
    }
    
    var flags: SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
      return .notReachable
    }
    
    if flags.contains(.reachable) == false {
      // The target host is not reachable.
      return .notReachable
    }
    else if flags.contains(.isWWAN) == true {
      // WWAN connections are OK if the calling application is using the CFNetwork APIs.
      return .reachableViaWWAN
    }
    else if flags.contains(.connectionRequired) == false {
      // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
      return .reachableViaWiFi
    }
    else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
      // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
      return .reachableViaWiFi
    }
    else {
      return .notReachable
    }
}



//Reachability
//declare this property where it won't go out of scope relative to your listener
fileprivate var reachability: Reachability!

protocol ReachabilityActionDelegate {
    func reachabilityChanged(_ isReachable: Bool)
}

protocol ReachabilityObserverDelegate: class, ReachabilityActionDelegate {
    func addReachabilityObserver() throws
    func removeReachabilityObserver()
}

// Declaring default implementation of adding/removing observer
extension ReachabilityObserverDelegate {
    
    /** Subscribe on reachability changing */
    func addReachabilityObserver() throws {
        reachability = try Reachability()
        
        reachability.whenReachable = { [weak self] reachability in
            self?.reachabilityChanged(true)
        }
        
        reachability.whenUnreachable = { [weak self] reachability in
            self?.reachabilityChanged(false)
        }
        
        try reachability.startNotifier()
    }
    
    /** Unsubscribe */
    func removeReachabilityObserver() {
        reachability.stopNotifier()
        reachability = nil
    }
}


func createImageThumbnail (_ image: UIImage) -> Data {
  
  let actualHeight:CGFloat = image.size.height
  let actualWidth:CGFloat = image.size.width
  let imgRatio:CGFloat = actualWidth/actualHeight
  let maxWidth:CGFloat = 150.0
  let resizedHeight:CGFloat = maxWidth/imgRatio
  let compressionQuality:CGFloat = 0.5
  
  let rect:CGRect = CGRect(x: 0, y: 0, width: maxWidth, height: resizedHeight)
  UIGraphicsBeginImageContext(rect.size)
  image.draw(in: rect)
  let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    let imageData: Data = img.jpegData(compressionQuality: compressionQuality)!
  UIGraphicsEndImageContext()
    return imageData
}






extension UIImageView {
    
    func showActivityIndicator(activityIndicator: UIActivityIndicatorView, backgroundView: UIView) {
    self.addSubview(backgroundView)
    self.addSubview(activityIndicator)
    activityIndicator.style = UIActivityIndicatorView.Style.medium
    activityIndicator.center = CGPoint(x: self.frame.size.width / 2, y: self.frame.size.height / 2)
    backgroundView.translatesAutoresizingMaskIntoConstraints = false
    backgroundView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
    backgroundView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
    backgroundView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    backgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    DispatchQueue.main.async {
      activityIndicator.startAnimating()
    }
  }
  
  func hideActivityIndicator(activityIndicator: UIActivityIndicatorView, backgroundView: UIView) {
    DispatchQueue.main.async {
      activityIndicator.stopAnimating()
    }
    
    activityIndicator.removeFromSuperview()
    backgroundView.removeFromSuperview()
  }
}


func tutorialRealizado(correo: String) {
    UserDefaults.standard.set(true, forKey: "tutorialCompletado")
    let db = Firestore.firestore()
    let docRef = db.collection("Usuarios").document(correo)
    docRef.updateData([
        "tutorialCompletado": true
    ]) { err in
        if let err = err {
            print("Error updating document: \(err)")
        } else {
            print("Document successfully updated")
        }
    }
}
