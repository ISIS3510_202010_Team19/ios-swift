//
//  RegistrarseViewController.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 26/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseFirestore

class RegistrarseViewController: UIViewController {
    
    //Outlets
    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var nombreErrorLabel: UILabel!
    @IBOutlet weak var apellidoTextField: UITextField!
    
    @IBOutlet weak var apellidoErrorLabel: UILabel!
    @IBOutlet weak var correoTextField: UITextField!
    @IBOutlet weak var correoErrorLabel: UILabel!
    @IBOutlet weak var contraseñaTextField: UITextField!
    @IBOutlet weak var telefonoTextField: UITextField!
    @IBOutlet weak var contraseñaErrorLabel: UILabel!
    @IBOutlet weak var completarRegistroBtn: UIButton!
    @IBOutlet weak var telefonoErrorLabel: UILabel!
    @IBOutlet weak var errorRegistroLabel: UILabel!
    //Toolbar teclado numérico
    let numberToolbar: UIToolbar = UIToolbar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
        // Do any additional setup after loading the view.
    }
    
    func setUpElements(){
        telefonoErrorLabel.isHidden = true
        nombreErrorLabel.isHidden = true
        nombreTextField.delegate = self
        nombreTextField.addTarget(self, action: #selector(checkAndDisplayError(textField:)), for: .editingChanged)
        nombreTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        apellidoErrorLabel.isHidden = true
        apellidoTextField.delegate = self
        apellidoTextField.addTarget(self, action: #selector(checkAndDisplayError(textField:)), for: .editingChanged)
        apellidoTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        correoTextField.delegate = self
        correoTextField.addTarget(self, action: #selector(checkAndDisplayError(textField:)), for: .editingChanged)
        correoTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        contraseñaTextField.delegate = self
        contraseñaTextField.addTarget(self, action: #selector(checkAndDisplayError(textField:)), for: .editingChanged)
        contraseñaTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        telefonoTextField.delegate = self
        telefonoTextField.addTarget(self, action: #selector(checkAndDisplayError(textField:)), for: .editingChanged)
        telefonoTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items=[UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil), UIBarButtonItem(title: "Apply", style: .done, target: self, action:#selector(self.hideKeyboard))
        ]
        numberToolbar.sizeToFit()
        telefonoTextField.inputAccessoryView = numberToolbar
        correoErrorLabel.isHidden = true
        contraseñaErrorLabel.isHidden = true
        errorRegistroLabel.isHidden = true
        Utilities.styleTextField(telefonoTextField)
        Utilities.styleTextField(nombreTextField)
        Utilities.styleTextField(apellidoTextField)
        Utilities.styleTextField(correoTextField)
        Utilities.styleTextField(contraseñaTextField)
        Utilities.styleFilledButton(completarRegistroBtn)
    }
    
    @objc func hideKeyboard () {
        let nextTag = telefonoTextField.tag + 1
        if let nextResponder = telefonoTextField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            telefonoTextField.resignFirstResponder()
        }
    }
    
    @objc func checkAndDisplayError (textField: UITextField){
        if textField == nombreTextField{
            if textField.text!.count >= 30{
                nombreErrorLabel.isHidden = false
                nombreErrorLabel.text = "Máx. 30 Carateres"
                nombreErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 30)
            }else{
                nombreErrorLabel.isHidden = true
            }
        } else if textField == apellidoTextField{
            if textField.text!.count >= 30{
                apellidoErrorLabel.isHidden = false
                apellidoErrorLabel.text = "Máx. 30 Carateres"
                apellidoErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 30)
            } else{
                apellidoErrorLabel.isHidden = true
            }
        } else if textField == correoTextField{
            if textField.text!.count >= 40{
                correoErrorLabel.isHidden = false
                correoErrorLabel.text = "Máx. 40 Carateres"
                correoErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 40)
            } else{
                correoErrorLabel.isHidden = true
            }
        } else if textField == telefonoTextField{
            if textField.text!.count >= 10{
                telefonoErrorLabel.isHidden = false
                telefonoErrorLabel.text = "Máx. 10 Carateres"
                telefonoErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 10)
                
            } else{
                telefonoErrorLabel.isHidden = true
            }
        } else if textField == contraseñaTextField{
            
            if textField.text!.count >= 30{
                contraseñaErrorLabel.isHidden = false
                contraseñaErrorLabel.text = "Máx. 30 Carateres"
                contraseñaErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 10)
                
            }else if !Utilities.isPasswordValid(contraseñaTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)) {
                contraseñaErrorLabel.text = "La contraseña debe tener mínimo 8 carateres, 1 caracter especial y 1 número."
                contraseñaErrorLabel.isHidden = false
                contraseñaErrorLabel.numberOfLines = 2
            }else{
                contraseñaErrorLabel.isHidden = true
            }
        }
    }

    func checkMaxLength(textField: UITextField!, maxLength: Int) {
        if (textField.text!.count > maxLength) {
            textField.deleteBackward()
        }
    }
    
    
    @IBAction func completarRegistroAction(_ sender: Any) {
        if nombreTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            apellidoTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            correoTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            contraseñaTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            let alert = UIAlertController(title: "Error",
            message: "Debes llenar todos los campos",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let cancelAction = UIAlertAction(title: "Ok",
              style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            alert.addAction(cancelAction)
            //Muestra la alerta
            present(alert,
            animated: true,
            completion: nil)
        } else{
            let nombre = nombreTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let apellido = apellidoTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let correo = correoTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let contraseña = contraseñaTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let telefono = telefonoTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            Auth.auth().createUser(withEmail: correo, password: contraseña) { (result,err) in
                if err != nil{
                    let alert = UIAlertController(title: "Error",
                    message: "Error creando un usuario.",
                    preferredStyle: .alert)
                    
                    //Creamos el UIAlertAction que nos permitirá volver
                    let cancelAction = UIAlertAction(title: "Ok",
                      style: .default) { (action: UIAlertAction) -> Void in
                    }
                    //Agrega el boton de volver a la alerta
                    alert.addAction(cancelAction)
                    //Muestra la alerta
                    self.present(alert,
                    animated: true,
                    completion: nil)
                } else{
                    let db = Firestore.firestore()
                    let data = ["nombre":nombre, "apellido":apellido,"telefono":telefono,"uid":result!.user.uid,"correo":correo]
                    db.collection("Usuarios").document(correo).setData(data) {(error) in
                        if error != nil{
                            let alert = UIAlertController(title: "Error",
                            message: "Error creando usuario.",
                            preferredStyle: .alert)
                            
                            //Creamos el UIAlertAction que nos permitirá volver
                            let cancelAction = UIAlertAction(title: "Ok",
                              style: .default) { (action: UIAlertAction) -> Void in
                            }
                            //Agrega el boton de volver a la alerta
                            alert.addAction(cancelAction)
                            //Muestra la alerta
                            self.present(alert,
                            animated: true,
                            completion: nil)
                        } else{
                            let alert = UIAlertController(title: "Usuario Creado",
                            message: "Tu usuario se ha creado exitosamente.",
                            preferredStyle: .alert)
                            
                            //Creamos el UIAlertAction que nos permitirá volver
                            let cancelAction = UIAlertAction(title: "Ok",
                              style: .default) { (action: UIAlertAction) -> Void in
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                let managedContext = appDelegate.persistentContainer.viewContext
                                UserDefaults.standard.set(false, forKey: "tutorialCompletado")
                                UserDefaults.standard.set(true, forKey: "empezarTutorial")
                                UserDefaults.standard.set(correo, forKey:"usuario")
                                syncDatosConUsuario(correo: correo, context: managedContext)
                                self.navigationController?.popViewController(animated: true)
                                let homeViewController = self.storyboard?.instantiateViewController(identifier: "HomeVC")
                                self.view.window?.rootViewController = homeViewController
                                self.view.window?.makeKeyAndVisible()
                            }
                            //Agrega el boton de volver a la alerta
                            alert.addAction(cancelAction)
                            //Muestra la alerta
                            self.present(alert,
                            animated: true,
                            completion: nil)
                        }
                    }
                }
                
            }
        }
        
        
    }
}


// MARK: - TextFields Registro Delegate
extension RegistrarseViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == telefonoTextField{
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }else {
            return true
        }
    }
    
}
