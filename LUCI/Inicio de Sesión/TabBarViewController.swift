//
//  TabBarViewController.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 6/05/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit
import FirebaseAuth

class TabBarViewController: UITabBarController {
    @IBOutlet weak var cerrarSesionBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func cerrarSesionAction(_ sender: Any) {
        try! Auth.auth().signOut()
        let homeViewController = self.storyboard?.instantiateViewController(identifier: "LoginVC")
        self.view.window?.rootViewController = homeViewController
        self.view.window?.makeKeyAndVisible()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
