//
//  IniciarSesionViewController.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 26/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import CoreData

class IniciarSesionViewController: UIViewController {
    @IBOutlet weak var correoTextField: UITextField!
    @IBOutlet weak var contraseñaTextField: UITextField!
    
    @IBOutlet weak var errorIniciandoLabel: UILabel!
    
    @IBOutlet weak var iniciarSesionBtn: UIButton!
    
    @IBOutlet weak var registrarseBtn: UIButton!
    @IBOutlet weak var correoErrorLabel: UILabel!
    
    @IBOutlet weak var contraseñaErrorLabel: UILabel!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpElements()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Auth.auth().currentUser != nil{
            UserDefaults.standard.set(Auth.auth().currentUser!.email!, forKey: "usuario")
            let homeViewController = self.storyboard?.instantiateViewController(identifier: "HomeVC")
            self.view.window?.rootViewController = homeViewController
            self.view.window?.makeKeyAndVisible()
            self.present(homeViewController!, animated: false, completion: nil)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let managedContext = appDelegate.persistentContainer.viewContext
            let usuario = Auth.auth().currentUser!.email!
            DispatchQueue.global().async{
                syncDatosConUsuario(correo: usuario, context: managedContext)
                syncClientes(usuario: usuario, context: managedContext)
                syncDomiciliarios(usuario: usuario, context: managedContext)
            }
            
        }
    }
    
    func setUpElements(){
            
        errorIniciandoLabel.isHidden = true
        correoErrorLabel.isHidden = true
        contraseñaErrorLabel.isHidden = true
        correoTextField.delegate = self
        contraseñaTextField.delegate = self
        Utilities.styleTextField(correoTextField)
        Utilities.styleTextField(contraseñaTextField)
        Utilities.styleFilledButton(registrarseBtn)
        Utilities.styleHollowButton(iniciarSesionBtn)
        if view.overrideUserInterfaceStyle == .dark {
            iniciarSesionBtn.tintColor = .white
        }
    }
    
    
    @objc func checkAndDisplayError (textField: UITextField){
        if textField == correoTextField{
            if textField.text!.count >= 30{
                correoErrorLabel.isHidden = false
                correoErrorLabel.text = "Máx. 30 Carateres"
                correoErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 30)
            }else{
                correoErrorLabel.isHidden = true
            }
        } else if textField == contraseñaTextField{
            if textField.text!.count >= 30{
                contraseñaErrorLabel.isHidden = false
                contraseñaErrorLabel.text = "Máx. 30 Carateres"
                contraseñaErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 30)
            } else{
                contraseñaErrorLabel.isHidden = true
            }
        }
    }

    func checkMaxLength(textField: UITextField!, maxLength: Int) {
        if (textField.text!.count > maxLength) {
            textField.deleteBackward()
        }
    }
    
    
    
    
    @IBAction func iniciarSesionAction(_ sender: Any) {
        if correoTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            contraseñaTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            let alert = UIAlertController(title: "Error",
            message: "Debes llenar todos los campos",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let cancelAction = UIAlertAction(title: "Ok",
              style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            alert.addAction(cancelAction)
            //Muestra la alerta
            present(alert,
            animated: true,
            completion: nil)
        }else{
            let correo = correoTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let contraseña = contraseñaTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            Auth.auth().signIn(withEmail: correo, password: contraseña) { (result, error) in
                if error != nil {
                    // Couldn't sign in
                    self.errorIniciandoLabel.isHidden = false
                    self.errorIniciandoLabel.text = "Hay un error en el usuario o la contraseña."+error!.localizedDescription
                    self.errorIniciandoLabel.numberOfLines = 8
                    self.contraseñaTextField.resignFirstResponder()
                }
                else {
                    UserDefaults.standard.set(correo, forKey: "usuario")
                    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                    DispatchQueue.global().async{
                        syncDatosConUsuario(correo: correo, context: managedContext)
                        syncClientes(usuario: correo, context: managedContext)
                        syncDomiciliarios(usuario: correo, context: managedContext)
                    }
                    let homeViewController = self.storyboard?.instantiateViewController(identifier: "HomeVC")
                    self.view.window?.rootViewController = homeViewController
                    self.view.window?.makeKeyAndVisible()
                }
            }
        }
    }
}

// MARK: - TextFields Registro Delegate
extension IniciarSesionViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
}
