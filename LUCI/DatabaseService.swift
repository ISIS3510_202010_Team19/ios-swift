//
//  DatabaseService.swift
//  LUCI
//
//  Created by David Bautista on 18/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import Foundation
import FirebaseDatabase
import CoreData
import FirebaseFirestore

class DatabaseService {
    
    static let shared = DatabaseService()
    
    private init(){}
    let usuarioId = UserDefaults.standard.string(forKey: "usuarioId")
    
    let pedidosReference = Database.database().reference().child("pedidos");
    
    let formatter = DateFormatter()

    func savePedido(pedido: Pedido, index: Int, context: NSManagedObjectContext) {
        formatter.dateFormat = "HH:mm E, d MMM y"
        
        var ped2:[String : Any?] = [
            "coreid": pedido.objectID.uriRepresentation().lastPathComponent,
            "estado": pedido.estado,
            "fechaCreacion": "sin fecha de creacion",
            "fechaEntrega": "sin fecha de entrega",
            "vozatexto": pedido.vozATexto ?? "sin vozatexto",
            "items": [],
            "cliente": nil,
            "domiciliario": nil
        ]
        
        var ped: [String : Any?] = [
            "coreid": pedido.objectID.uriRepresentation().lastPathComponent,
            "estado": pedido.estado,
            "fechaCreacion": "sin fecha de creacion",
            "fechaEntrega": "sin fecha de entrega",
            "vozatexto": pedido.vozATexto ?? "sin vozatexto",
            "items": [],
            "cliente": nil,
            "domiciliario": nil
        ]
        
        
        if pedido.fechaCreacion != nil {
            ped["fechaCreacion"] = formatter.string(from: pedido.fechaCreacion!)
            ped2["fechaCreacion"] = formatter.string(from: pedido.fechaCreacion!)
        }
        
        if pedido.fechaEntrega != nil {
            ped["fechaEntrega"] = formatter.string(from: pedido.fechaEntrega!)
            ped2["fechaCreacion"] = formatter.string(from: pedido.fechaCreacion!)
        }
        
        
        if pedido.cliente != nil {
            ped["client"] =
                [
                    "coreid": pedido.cliente!.objectID.uriRepresentation().lastPathComponent,
                    "nombre": pedido.cliente!.nombre ?? "sin nombre",
                    "apellido": pedido.cliente!.apellido ?? "sin apellido",
                    "direccion": pedido.cliente!.direccion ?? "sin direccion",
                    "telefono": pedido.cliente!.telefono ]
        }
        
        if pedido.domiciliario != nil {
            ped["domiciliario"] = [
                "coreid": pedido.domiciliario!.objectID.uriRepresentation().lastPathComponent,
                "nombre": pedido.domiciliario!.nombre ?? "sin nombre",
                "apellido": pedido.domiciliario!.apellido ?? "sin apellido",
                "telefono": pedido.domiciliario!.telefono
            ]
        }
        
        if pedido.items != nil {
            var items: [[String : Any]] = []
            
            for it in pedido.items?.allObjects as! [Item] {
                let item : [String: Any] = [
                    "coreid": it.objectID.uriRepresentation().lastPathComponent,
                    "cantidad": it.cantidad,
                    "nombre": it.nombre ?? "sin nombre",
                ]
                
                items.append(item)
            }
            ped["items"] = items
        }
        
        print("listo para crear pedido \(index) en firebase")
        
        pedidosReference.childByAutoId().setValue(ped) {
            (error: Error?, ref: DatabaseReference) -> Void in
            if let error = error {
                print("error firebase: \(error)")
            } else {
                print("\(index) object saved in firebase!", ref)
                pedido.guardadoRemoto = true;
                do {
                    try context.save()
                } catch {
                    print("falla guardando estado pedido-firebase true")
                }
            }
            
            
        }
    }
}
