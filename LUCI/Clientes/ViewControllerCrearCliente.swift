//
//  ViewControllerCrearCliente.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 13/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit
import CoreData

class ViewControllerCrearCliente: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    //Outlets
    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var nombreErrorLabel: UILabel!
    @IBOutlet weak var apellidoTextField: UITextField!
    @IBOutlet weak var direccionTextField: UITextField!
    @IBOutlet weak var telefonoTextField: UITextField!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var apellidoErrorLabel: UILabel!
    @IBOutlet weak var direccionErrorLabel: UILabel!
    @IBOutlet weak var telefonoErrorLabel: UILabel!
    
    
    //Clientes DB
    var clientesDB = [NSManagedObject]()
    let usuario = UserDefaults.standard.string(forKey: "usuario")!
    
    //Para la foto del usuario
    var imagePicker = UIImagePickerController()
    
    //Toolbar teclado numérico
    let numberToolbar: UIToolbar = UIToolbar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tutorialCompletado = UserDefaults.standard.bool(forKey: "tutorialCompletado")
        if tutorialCompletado == false{
            UserDefaults.standard.set(3, forKey:"tutoCount")
            let tutorial = UIAlertController(title: "Tutorial",
            message: "Acá puedes agregar un nuevo cliente. Por favor llena los campos correctamente y presiona el botón agregar cliente.",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                       style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            tutorial.addAction(ok)
            //Muestra la alerta
            present(tutorial,
            animated: true,
            completion: nil)
        }
        // Label errores
        nombreErrorLabel.isHidden = true
        apellidoErrorLabel.isHidden = true
        direccionErrorLabel.isHidden = true
        telefonoErrorLabel.isHidden = true
        
        // Formulario crear cliente
        nombreTextField.placeholder = "Pepito"
        nombreTextField.delegate = self
        nombreTextField.addTarget(self, action: #selector(checkAndDisplayError(textField:)), for: .editingChanged)
        nombreTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        apellidoTextField.placeholder = "Perez"
        apellidoTextField.delegate = self
        apellidoTextField.addTarget(self, action: #selector(checkAndDisplayError(textField:)), for: .editingChanged)
        apellidoTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        direccionTextField.placeholder = "Cra. 13 # 152 - 73"
        direccionTextField.delegate = self
        direccionTextField.addTarget(self, action: #selector(checkAndDisplayError(textField:)), for: .editingChanged)
        direccionTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        telefonoTextField.placeholder = "316 222 83 25"
        telefonoTextField.delegate = self
        telefonoTextField.addTarget(self, action: #selector(checkAndDisplayError(textField:)), for: .editingChanged)
        telefonoTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items=[UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil), UIBarButtonItem(title: "Apply", style: .done, target: self, action:#selector(self.hideKeyboard))
        ]
        numberToolbar.sizeToFit()
        telefonoTextField.inputAccessoryView = numberToolbar
        
        // Do any additional setup after loading the view.
    }
    
    @objc func checkAndDisplayError (textField: UITextField){
        if textField == nombreTextField{
            if textField.text!.count >= 30{
                nombreErrorLabel.isHidden = false
                nombreErrorLabel.text = "Máx. 30 Carateres"
                nombreErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 30)
            } else{
                nombreErrorLabel.isHidden = true
            }
        } else if textField == apellidoTextField{
            if textField.text!.count >= 30{
                apellidoErrorLabel.isHidden = false
                apellidoErrorLabel.text = "Máx. 30 Carateres"
                apellidoErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 30)
            } else{
                apellidoErrorLabel.isHidden = true
            }
        } else if textField == direccionTextField{
            if textField.text!.count >= 40{
                direccionErrorLabel.isHidden = false
                direccionErrorLabel.text = "Máx. 40 Carateres"
                direccionErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 40)
            } else{
                direccionErrorLabel.isHidden = true
            }
        } else if textField == telefonoTextField{
            if textField.text!.count >= 10{
                telefonoErrorLabel.isHidden = false
                telefonoErrorLabel.text = "Máx. 10 Carateres"
                telefonoErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 10)
                
            } else{
                telefonoErrorLabel.isHidden = true
            }
        }
    }
    
    @objc func hideKeyboard () {
        let nextTag = telefonoTextField.tag + 1
        if let nextResponder = telefonoTextField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            telefonoTextField.resignFirstResponder()
        }
    }
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }

        // print out the image size as a test
        previewImageView.image = image
        print(image.size)
    }
    
    
    //Agregar foto cliente
    @IBAction func agregarFotoButton(_ sender: Any) {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
        
    }
    
    //Tomar foto cliente
    @IBAction func tomarFotoButton(_ sender: Any) {
        if !UIImagePickerController.isSourceTypeAvailable(.camera){

            let alertController = UIAlertController.init(title: nil, message: "El dispositivo no cuenta con una cámara válida.", preferredStyle: .alert)

            let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: {(alert: UIAlertAction!) in
            })

            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)

        }else{
             let vc = UIImagePickerController()
             vc.sourceType = .camera
             vc.allowsEditing = true
             vc.delegate = self
             present(vc, animated: true)
        }
    }
    @IBAction func agregarClienteButton(_ sender: Any) {
        let nombre = nombreTextField.text
        let direccion = direccionTextField.text
        let telefono = telefonoTextField.text
        let apellido = apellidoTextField.text
        let foto = previewImageView.image
        if (nombre?.isEmpty == true || direccion?.isEmpty == true || telefono?.isEmpty == true || apellido?.isEmpty == true || foto == nil){
            //Crea la alerta
            let alert = UIAlertController(title: "Error",
            message: "Debes llenar todos los campos",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let cancelAction = UIAlertAction(title: "Ok",
              style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            alert.addAction(cancelAction)
            //Muestra la alerta
            present(alert,
            animated: true,
            completion: nil)
        }else{
            guardarClienteDB(nombre: nombre!, apellido: apellido!, direccion: direccion!, telefono: Int(telefono!)!, foto: createImageThumbnail(foto!))
        }
        
    }
    
    func checkMaxLength(textField: UITextField!, maxLength: Int) {
        if (textField.text!.count > maxLength) {
            textField.deleteBackward()
        }
    }
    
    
    //MARK: - Guardar cliente en BD
    func guardarClienteDB(nombre:String, apellido:String, direccion:String, telefono:Int, foto:Data){
        //1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        //2
        let entity = NSEntityDescription.entity(forEntityName: "Cliente", in: managedContext)
        let cliente = NSManagedObject(entity: entity!, insertInto: managedContext)
        //3
        cliente.setValue(nombre, forKey: "nombre")
        cliente.setValue(direccion, forKey: "direccion")
        cliente.setValue(telefono, forKey: "telefono")
        cliente.setValue(apellido, forKey: "apellido")
        cliente.setValue(foto, forKey: "foto")
        cliente.setValue(true, forKey: "favorito")
        cliente.setValue(usuario, forKey: "usuario")
        cliente.setValue(false, forKey: "guardadoRemoto")
        //4
        do {
            try managedContext.save()
            //5
            clientesDB.append(cliente)
            print("GUARDADO EN BD")
            let guardadoExitoso = UIAlertController(title: "Guardado Exitoso",
            message: "El cliente ha sido guardado.",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let volver = UIAlertAction(title: "Ok",
                                       style: .default) { (action: UIAlertAction) -> Void in
                                        self.navigationController?.popViewController(animated: true)
            }
            //Agrega el boton de volver a la alerta
            guardadoExitoso.addAction(volver)
            //Muestra la alerta
            present(guardadoExitoso,
            animated: true,
            completion: nil)
            
        } catch let error as NSError {
        print("No ha sido posible guardar \(error), \(error.userInfo)")
      }
        DispatchQueue.global().async {
            subirClientes(usuario: self.usuario, context: managedContext)
        }
    }
    
}


// MARK: - TextFields CrearCliente Delegate
extension ViewControllerCrearCliente: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print(nombreTextField.text!)
        print(direccionTextField.text!)
        print(telefonoTextField.text!)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == telefonoTextField{
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }else {
            return true
        }
    }
    
}
