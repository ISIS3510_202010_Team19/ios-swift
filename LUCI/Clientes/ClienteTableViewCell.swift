//
//  ClienteTableViewCell.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 13/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit

class ClienteTableViewCell: UITableViewCell {

    // Outlets
    @IBOutlet weak var clienteImageView: UIImageView!    
    @IBOutlet weak var nombreClienteLabel: UILabel!
    @IBOutlet weak var ultimoPedidoLabel: UILabel!
    @IBOutlet weak var direccionClienteLabel: UILabel!
    @IBOutlet weak var telefonoClienteLabel: UILabel!
    @IBOutlet weak var llamarButton: UIButton!
    @IBOutlet weak var mensajeButton: UIButton!
    
    var fondo: UIView = {
      let backgroundView = UIView()
      backgroundView.backgroundColor = UIColor.black
      backgroundView.alpha = 0.8
      backgroundView.layer.cornerRadius = 0
      backgroundView.layer.masksToBounds = true
      
      return backgroundView
    }()
    
    var activityIndicator: UIActivityIndicatorView = {
        var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
      activityIndicator.hidesWhenStopped = true
      activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        activityIndicator.style = UIActivityIndicatorView.Style.large
      activityIndicator.autoresizingMask = [.flexibleLeftMargin , .flexibleRightMargin , .flexibleTopMargin , .flexibleBottomMargin]
      activityIndicator.isUserInteractionEnabled = false
      return activityIndicator
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        llamarButton.setTitle("", for: .normal)
        llamarButton.setImage(UIImage(named: "Llamar Icon"), for: .normal)
        mensajeButton.setTitle("", for: .normal)
        mensajeButton.setImage(UIImage(named: "Mensaje Icon"), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
