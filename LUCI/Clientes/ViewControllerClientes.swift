//
//  ViewControllerClientes.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 13/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit
import CoreData

class ViewControllerClientes: UIViewController {
    
    // Outlets tabla clientes
    @IBOutlet weak var clientesTableView: UITableView!
    @IBOutlet weak var añadirClienteButton: UIButton!
    
    // DB Clientes
    var setClientes = [Cliente]()
    var clienteSeleccionado: Cliente?
    // Datos para mostrar clientes
    private let myImages:[UIImage?] = [UIImage(named: "User 1"), UIImage(named: "User 2"), UIImage(named: "User 3")]
    private let myNombres = ["User 1","User 2","User 3"]
    private let myUltimosPedidos = ["2/Febrero","3/Febrero","20/Abril"]
    private let myDirecciones = ["Cra 13 # 25 - 35", "Calle 34 # 22 -45", "Calle 166 # 54 - 29"]
    private let myTelefonos = ["+57 317 222 98 23", "+57 316 283 98 78", "+57 300 631 23 56"]
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh), name: NSNotification.Name(rawValue: "newDataNotif"), object: nil)
        // Button Añadir Cliente
        //añadirClienteButton.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        
        // Table View Clientes
        clientesTableView.dataSource = self
        clientesTableView.tableFooterView = UIView()
        clientesTableView.delegate = self
        clientesTableView.register(UINib(nibName: "ClienteTableViewCell", bundle: nil), forCellReuseIdentifier: "clienteTVC")
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let tutorialCompletado = UserDefaults.standard.bool(forKey: "tutorialCompletado")
        let estadoTuto = UserDefaults.standard.integer(forKey: "tutoCount")
        if tutorialCompletado == false{
            if estadoTuto == 2{
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Acá puedes ver todos los clientes creados, presiona el botón de crear cliente en la parte superior para crear un nuevo cliente.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            }else{
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Ahora ya puedes ver tu cliente creado, ahora finalmente vamos a crear un pedido nuevo accediendo a ellos en el menú inferior.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                                            
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            }
        }
        setClientes = [Cliente]()
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
        let result = try? managedObjectContext.fetch(fetchRequest)
        let resultData = result as! [Cliente]
        for object in resultData{
            setClientes.append(object)
        }
        clientesTableView.reloadData()
        
    }
    
    // Muestra pantalla de agregar cliente
    @IBAction func agregarClienteAction(_ sender: Any) {
        print("Se presionó el botón agregar cliente")
        performSegue(withIdentifier: "VCLAgregarCliente", sender: self)
    }
    
    @objc func refresh(notification:NSNotification) {
        setClientes = [Cliente]()
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
        let result = try? managedObjectContext.fetch(fetchRequest)
        let resultData = result as! [Cliente]
        for object in resultData{
            setClientes.append(object)
        }
        clientesTableView.reloadData()
    }
}




//MARK: -ClientesTableView DataSource
extension ViewControllerClientes: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return setClientes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "clienteTVC") as? ClienteTableViewCell
        let cliente = setClientes[indexPath.row]
        
        let nombre = cliente.nombre!
        let apellido = cliente.apellido!
        let direccion = cliente.direccion!
        let telefono = cliente.telefono
        if cliente.foto != nil{
            let fotoData = cliente.foto!
            let foto = UIImage(data: fotoData)
            cell?.clienteImageView.image = foto!
            cell?.clienteImageView.hideActivityIndicator(activityIndicator: cell!.activityIndicator, backgroundView: cell!.fondo)
        }else{
            cell?.clienteImageView.showActivityIndicator(activityIndicator: cell!.activityIndicator, backgroundView: cell!.fondo)
        }
        cell?.nombreClienteLabel.text = "\(nombre) \(apellido)"
        cell?.ultimoPedidoLabel.text = myUltimosPedidos[0]
        cell?.direccionClienteLabel.text = "\(direccion)"
        cell?.telefonoClienteLabel.text = "\(telefono)"
        
        cell?.clienteImageView.layer.cornerRadius = (cell?.clienteImageView.frame.size.width)! / 2
        cell?.clienteImageView.clipsToBounds = true
        cell?.accessoryType = .disclosureIndicator
        return cell!
    }
    
    
}

//MARK: -ClientesTableView Delegate
extension ViewControllerClientes: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        clienteSeleccionado = setClientes[indexPath.row]
        performSegue(withIdentifier: "VClienteDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VClienteDetail"{
            if let destino = segue.destination as? ClienteDetailViewController{
                destino.cliente = clienteSeleccionado!
                
            }
        }
    }
    
}
