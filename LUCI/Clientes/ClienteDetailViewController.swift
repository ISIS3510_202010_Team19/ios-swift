//
//  ClienteDetailViewController.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 14/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit
import CoreData

class ClienteDetailViewController: UIViewController {
    
    var cliente = Cliente()
    var pedidoSeleccionado: Pedido?
    
    
    //Outlets
    @IBOutlet weak var fotoImageView: UIImageView!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var direccionLabel: UILabel!
    @IBOutlet weak var telefonoLabel: UILabel!
    @IBOutlet weak var numeroPedidosLabel: UILabel!
    @IBOutlet weak var articulosFrecuentesTableView: UITableView!
    @IBOutlet weak var pedidosTableView: UITableView!
    @IBOutlet weak var favoritosBtn: UIButton!
    
    var setPedidos = [Pedido]()
    var setItemsNombre = [String]()
    var cantItems = [String:Int]()
    let itemsImages: [String: UIImage?] = ["Aguacate":UIImage(named: "aguacate"),"Manzana":UIImage(named: "manzana"),"Pera":UIImage(named: "pera"),"Mango":UIImage(named: "mango"),"Papa":UIImage(named: "papa"),"Guayaba":UIImage(named: "guayaba"),"Coco":UIImage(named: "coco"),"Durazno":UIImage(named: "durazno")]
    
    
    var fondo: UIView = {
      let backgroundView = UIView()
      backgroundView.backgroundColor = UIColor.black
      backgroundView.alpha = 0.8
      backgroundView.layer.cornerRadius = 0
      backgroundView.layer.masksToBounds = true
      
      return backgroundView
    }()
    
    var activityIndicator: UIActivityIndicatorView = {
        var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
      activityIndicator.hidesWhenStopped = true
      activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        activityIndicator.style = UIActivityIndicatorView.Style.large
      activityIndicator.autoresizingMask = [.flexibleLeftMargin , .flexibleRightMargin , .flexibleTopMargin , .flexibleBottomMargin]
      activityIndicator.isUserInteractionEnabled = false
      return activityIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh), name: NSNotification.Name(rawValue: "newDataNotif"), object: nil)
        let nombre = cliente.nombre!
        let apellido = cliente.apellido!
        let direccion = cliente.direccion!
        let telefono = cliente.telefono
        if cliente.foto != nil{
            let fotoData = cliente.foto!
            let foto = UIImage(data: fotoData)
            fotoImageView.image = foto
            fotoImageView.hideActivityIndicator(activityIndicator: activityIndicator, backgroundView: fondo)
        }else{
            fotoImageView.showActivityIndicator(activityIndicator: activityIndicator, backgroundView: fondo)
        }
        
        var numeroPedidos = "No ha realizado pedidos"
        if cliente.pedidos?.count != 0{
            let num = cliente.pedidos?.count
            numeroPedidos = "\(num!)"
        }
        
        fotoImageView.layer.cornerRadius = fotoImageView.frame.size.width / 2
        fotoImageView.clipsToBounds = true
        nombreLabel.text = "\(nombre) \(apellido)"
        direccionLabel.text = direccion
        telefonoLabel.text = "\(telefono)"
        numeroPedidosLabel.text = numeroPedidos
        // Do any additional setup after loading the view.
        
        //Pedidos TableView
        pedidosTableView.dataSource = self
        pedidosTableView.tableFooterView = UIView()
        pedidosTableView.delegate = self
        pedidosTableView.register(UINib(nibName:  "PedidoTableViewCell", bundle: nil), forCellReuseIdentifier: "pedidoTVC")
        
        //ArtículosFrecuentes TableView
        articulosFrecuentesTableView.dataSource = self
        articulosFrecuentesTableView.tableFooterView = UIView()
        articulosFrecuentesTableView.register(UINib(nibName: "CarritoTableViewCell", bundle: nil), forCellReuseIdentifier: "carritoTVC")
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if cliente.favorito == true{
            favoritosBtn.setTitle("Quitar de Favoritos", for: .normal)
        }else{
            favoritosBtn.setTitle("Añadir a Favoritos", for: .normal)
        }
        setItemsNombre = [String]()
        setPedidos = [Pedido]()
        cantItems = [String:Int]()
        let pedidos = cliente.pedidos!.allObjects as! [Pedido]
        for p in pedidos{
            setPedidos.append(p)
            let itemsPedido = p.items!.allObjects as! [Item]
            for it in itemsPedido{
                if (cantItems[it.nombre!] != nil){
                    cantItems[it.nombre!]! += Int(it.cantidad)
                }else{
                    cantItems[it.nombre!] = Int(it.cantidad)
                }
            }
        }
        setItemsNombre = [String] (cantItems.keys)
        pedidosTableView.reloadData()
        articulosFrecuentesTableView.reloadData()
    }
    
    @objc func refresh(notification:NSNotification) {
        setItemsNombre = [String]()
        setPedidos = [Pedido]()
        cantItems = [String:Int]()
        let pedidos = cliente.pedidos!.allObjects as! [Pedido]
        for p in pedidos{
            setPedidos.append(p)
            let itemsPedido = p.items!.allObjects as! [Item]
            for it in itemsPedido{
                if (cantItems[it.nombre!] != nil){
                    cantItems[it.nombre!]! += Int(it.cantidad)
                }else{
                    cantItems[it.nombre!] = Int(it.cantidad)
                }
            }
        }
        setItemsNombre = [String] (cantItems.keys)
        pedidosTableView.reloadData()
        articulosFrecuentesTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VCPedidoDetail"{
            if let destino = segue.destination as? PedidoDetailViewController{
                destino.pedido = pedidoSeleccionado!
            }
        }
    }
    
    
    @IBAction func favoritoAction(_ sender: Any) {
        var tit = "Agregado a Favoritos"
        var mens = "Se ha agregado el cliente \(cliente.nombre!) a favoritos"
        var butT = "Agregar a favoritos"
        if (cliente.favorito == false){
            //favoritosBtn.setTitle("Añadir a favoritos", for: .normal)
            cliente.favorito = true
            butT = "Quitar de favoritos"
        }else{
            //favoritosBtn.setTitle("Quitar de favoritos", for: .normal)
            cliente.favorito = false
            tit = "Quitado de Favoritos"
            mens = "Se ha quitado el cliente \(cliente.nombre!) de favoritos"
            
        }
        self.favoritosBtn.setTitle(butT, for: .normal)
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            try managedObjectContext.save()
            let domiciliarioEnCamino = UIAlertController(title: tit, message: mens, preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                       style: .default) { (action: UIAlertAction) -> Void in
                                        
            }
            //Agrega el boton de volver a la alerta
            domiciliarioEnCamino.addAction(ok)
            //Muestra la alerta
            present(domiciliarioEnCamino,
            animated: true,
            completion: nil)
            
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    
    
}

//MARK: - PedidosTableView / ArticulosTableView DataSource
extension ClienteDetailViewController: UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == pedidosTableView{
            return setPedidos.count
        }else{
            return setItemsNombre.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == pedidosTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "pedidoTVC") as? PedidoTableViewCell
            let nombre = setPedidos[indexPath.row].cliente?.nombre
            let apellido = setPedidos[indexPath.row].cliente?.apellido
            if setPedidos[indexPath.row].cliente?.foto != nil{
                let imagen = UIImage(data: (setPedidos[indexPath.row].cliente?.foto)!)
                cell?.clienteImageView.image = imagen
                cell?.clienteImageView.hideActivityIndicator(activityIndicator: cell!.activityIndicator, backgroundView: cell!.fondo)
            }else{
                cell?.clienteImageView.showActivityIndicator(activityIndicator: cell!.activityIndicator, backgroundView: cell!.fondo)
            }
            
            cell?.nombreClienteLabel.text = "\(nombre!) \(apellido!)"
            cell?.horaPedidoLabel.text = "Pedido para las 12:00pm de hoy"
            
            switch setPedidos[indexPath.row].estado {
            //Procesado
            case 1:
                cell?.relojProgressView.progress = 1
                cell?.edProgressView.progress = 0
                cell?.relojEstadoImageView.tintColor = UIColor(named: "Accent1")
                cell?.edEstadoImageView.tintColor = UIColor(named: "Accent3")
            //Procesado y entregado al domiciliario
            case 2:
                cell?.relojProgressView.progress = 1
                cell?.edProgressView.progress = 0.2
                cell?.relojEstadoImageView.tintColor = UIColor(named: "Accent1")
                cell?.edEstadoImageView.tintColor = UIColor(named: "Accent2")
            //En camino al cliente
            case 3:
                cell?.relojProgressView.progress = 1
                cell?.edProgressView.progress = 0.5
                cell?.relojEstadoImageView.tintColor = UIColor(named: "Accent1")
                cell?.edEstadoImageView.tintColor = UIColor(named: "Accent2")
            default:
                cell?.relojProgressView.progress = 1
                cell?.edProgressView.progress = 1
                cell?.relojEstadoImageView.tintColor = UIColor(named: "Accent1")
                cell?.edEstadoImageView.tintColor = UIColor(named: "Accent1")
            }
            return cell!
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "carritoTVC") as? CarritoTableViewCell
            let nombre = setItemsNombre[indexPath.row]
            cell?.nombreLabel.text = nombre
            cell?.itemImageView.image = itemsImages[nombre]!
            let cantidad = cantItems[nombre]!
            cell?.cantidadLabel.text = String(cantidad)
            return cell!
        }
        
        /*var a = setPedidos[0].allObjects as? Pedido
        
        cell?.nombreClienteLabel.text = setPedidos[indexPath.row]*/
    }
}

//MARK: - PedidosTableView Delegate
extension ClienteDetailViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pedidoSeleccionado = setPedidos[indexPath.row]
        performSegue(withIdentifier: "VCPedidoDetail", sender: self)
    }
    
    
}
