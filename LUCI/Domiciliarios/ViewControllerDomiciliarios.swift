//
//  ViewControllerDomiciliarios.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 16/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit
import CoreData

class ViewControllerDomiciliarios: UIViewController {
    
    //Outlets
    @IBOutlet weak var domiciliariosTableView: UITableView!
    
    // DB Domiciliarios
    var setDomiciliarios = [Domiciliario]()
    var domiciliarioSeleccionado: Domiciliario?
    
    var fondo: UIView = {
      let backgroundView = UIView()
      backgroundView.backgroundColor = UIColor.black
      backgroundView.alpha = 0.8
      backgroundView.layer.cornerRadius = 0
      backgroundView.layer.masksToBounds = true
      
      return backgroundView
    }()
    
    var activityIndicator: UIActivityIndicatorView = {
        var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
      activityIndicator.hidesWhenStopped = true
      activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        activityIndicator.style = UIActivityIndicatorView.Style.large
      activityIndicator.autoresizingMask = [.flexibleLeftMargin , .flexibleRightMargin , .flexibleTopMargin , .flexibleBottomMargin]
      activityIndicator.isUserInteractionEnabled = false
      return activityIndicator
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh), name: NSNotification.Name(rawValue: "newDataNotif"), object: nil)
        domiciliariosTableView.dataSource = self
        domiciliariosTableView.delegate = self
        domiciliariosTableView.tableFooterView = UIView()
        domiciliariosTableView.register(UINib(nibName: "ClienteTableViewCell", bundle: nil), forCellReuseIdentifier: "clienteTVC")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let tutorialCompletado = UserDefaults.standard.bool(forKey: "tutorialCompletado")
        let estadoTuto = UserDefaults.standard.integer(forKey: "tutoCount")
        if tutorialCompletado == false{
            if estadoTuto == 1{
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Acá puedes ver todos los domiciliarios creados, presiona el botón de crear domiciliario en la parte superior para crear un nuevo domiciliario.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            } else if estadoTuto == 5{
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Cuando regrese tu domiciliario accede de nuevo al pedido para finalizarlo, para este tutorial ingresa al pedido creado inmediatamente.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                                            self.navigationController?.popViewController(animated: true)
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            }else if estadoTuto == 6{
                // Override point for customization after application launch.
                // increment received number by one
                UserDefaults.standard.set(true, forKey:"tutorialCompletado")
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Felicidades!! Has completado todo el tutorial, ahora estás listo para usar a LUCI.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                                            self.navigationController?.popViewController(animated: true)
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            }
                
            else{
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Ahora ya puedes ver tu domiciliario creado, vamos a crear un cliente nuevo accediendo al menú de clientes en el menú inferior.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                                            self.navigationController?.popViewController(animated: true)
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            }
        }
        setDomiciliarios = [Domiciliario]()
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Domiciliario")
        let result = try? managedObjectContext.fetch(fetchRequest)
        let resultData = result as! [Domiciliario]
        for object in resultData{
            setDomiciliarios.append(object)
        }
        domiciliariosTableView.reloadData()
    }
    
    @objc func refresh(notification:NSNotification) {
        setDomiciliarios = [Domiciliario]()
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Domiciliario")
        let result = try? managedObjectContext.fetch(fetchRequest)
        let resultData = result as! [Domiciliario]
        for object in resultData{
            setDomiciliarios.append(object)
        }
        domiciliariosTableView.reloadData()
    }
    
    @IBAction func añadirDomiciliario(_ sender: Any) {
        performSegue(withIdentifier: "VCCrearDomiciliario", sender: self)
    }
    
}



//MARK: -DomiciliariosTableView DataSource
extension ViewControllerDomiciliarios: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return setDomiciliarios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "clienteTVC") as? ClienteTableViewCell
        let domiciliario = setDomiciliarios[indexPath.row]
        
        let nombre = domiciliario.nombre!
        let apellido = domiciliario.apellido!
        let telefono = domiciliario.telefono
        if domiciliario.foto != nil{
            let fotoData = domiciliario.foto!
            let foto = UIImage(data: fotoData)
            cell?.clienteImageView.image = foto!
            cell?.clienteImageView.hideActivityIndicator(activityIndicator: cell!.activityIndicator, backgroundView: cell!.fondo)
            
        }else{
            cell?.clienteImageView.showActivityIndicator(activityIndicator: cell!.activityIndicator, backgroundView: cell!.fondo)
        }
        cell?.clienteImageView.layer.cornerRadius = (cell?.clienteImageView.frame.size.width)! / 2
        cell?.clienteImageView.clipsToBounds = true
        cell?.nombreClienteLabel.text = "\(nombre) \(apellido)"
        cell?.ultimoPedidoLabel.text = "Hora para salir: 4:32pm"
        cell?.direccionClienteLabel.text = "Hora de vuelta: 5:30pm"
        cell?.telefonoClienteLabel.text = "\(telefono)"
        cell?.accessoryType = .disclosureIndicator
        return cell!
    }
    
    
}

//MARK: -DomiciliariosTableView Delegate
extension ViewControllerDomiciliarios: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        domiciliarioSeleccionado = setDomiciliarios[indexPath.row]
        performSegue(withIdentifier: "VCDomiciliarioDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VCDomiciliarioDetail"{
            if let destino = segue.destination as? DomiciliarioDetailViewController{
                destino.domiciliario = domiciliarioSeleccionado!
            }
        }
    }
    
}
