//
//  ViewControllerAgregarDomiciliario.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 16/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit
import CoreData
import FirebaseFirestore

class ViewControllerAgregarDomiciliario: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var nombreErrorLabel: UILabel!
    @IBOutlet weak var apellidoErrorLabel: UILabel!
    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var telefonoErrorLabel: UILabel!
    @IBOutlet weak var apellidoTextField: UITextField!
    @IBOutlet weak var telefonoTextField: UITextField!
    @IBOutlet weak var previewImageView: UIImageView!
    
    //Correo (Id Usuario)
    let userId = UserDefaults.standard.string(forKey: "userId")
    let usuario = UserDefaults.standard.string(forKey: "usuario")!
    
    //Domiciliarios DB
    var domiciliariosDB = [NSManagedObject]()
    
    //Para la foto del usuario
    var imagePicker = UIImagePickerController()
    
    //Toolbar teclado numérico
    let numberToolbar: UIToolbar = UIToolbar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tutorialCompletado = UserDefaults.standard.bool(forKey: "tutorialCompletado")
        if tutorialCompletado == false{
            UserDefaults.standard.set(2, forKey:"tutoCount")
            let tutorial = UIAlertController(title: "Tutorial",
            message: "Acá puedes agregar un nuevo domiciliario. Por favor llena los campos correctamente y presiona el botón agregar domiciliario.",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                       style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            tutorial.addAction(ok)
            //Muestra la alerta
            present(tutorial,
            animated: true,
            completion: nil)
        }
        
        
        // Label errores
        nombreErrorLabel.isHidden = true
        apellidoErrorLabel.isHidden = true
        telefonoErrorLabel.isHidden = true
        
        nombreTextField.placeholder = "Pepito"
        nombreTextField.delegate = self
        nombreTextField.addTarget(self, action: #selector(checkAndDisplayError(textField:)), for: .editingChanged)
        nombreTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        apellidoTextField.placeholder = "Perez"
        apellidoTextField.delegate = self
        apellidoTextField.addTarget(self, action: #selector(checkAndDisplayError(textField:)), for: .editingChanged)
        apellidoTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        telefonoTextField.placeholder = "316 222 83 25"
        telefonoTextField.delegate = self
        telefonoTextField.addTarget(self, action: #selector(checkAndDisplayError(textField:)), for: .editingChanged)
        telefonoTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items=[UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil), UIBarButtonItem(title: "Apply", style: .done, target: self, action:#selector(self.hideKeyboard))
        ]
        numberToolbar.sizeToFit()
        telefonoTextField.inputAccessoryView = numberToolbar
        // Do any additional setup after loading the view.
    }
    
    @objc func hideKeyboard () {
        telefonoTextField.resignFirstResponder()
    }
    
    @objc func checkAndDisplayError (textField: UITextField){
        if textField == nombreTextField{
            if textField.text!.count >= 30{
                nombreErrorLabel.isHidden = false
                nombreErrorLabel.text = "Máx. 30 Carateres"
                nombreErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 30)
            } else{
                nombreErrorLabel.isHidden = true
            }
        } else if textField == apellidoTextField{
            if textField.text!.count >= 30{
                apellidoErrorLabel.isHidden = false
                apellidoErrorLabel.text = "Máx. 30 Carateres"
                apellidoErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 30)
            } else{
                apellidoErrorLabel.isHidden = true
            }
        } else if textField == telefonoTextField{
            if textField.text!.count >= 10{
                telefonoErrorLabel.isHidden = false
                telefonoErrorLabel.text = "Máx. 10 Carateres"
                telefonoErrorLabel.textColor = .red
                checkMaxLength(textField: textField, maxLength: 10)
                
            } else{
                telefonoErrorLabel.isHidden = true
            }
        }
    }
    
    func checkMaxLength(textField: UITextField!, maxLength: Int) {
        if (textField.text!.count > maxLength) {
            textField.deleteBackward()
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }

        // print out the image size as a test
        previewImageView.image = image
        print(image.size)
    }
    
    @IBAction func tomarFotoAction(_ sender: Any) {
        if !UIImagePickerController.isSourceTypeAvailable(.camera){

            let alertController = UIAlertController.init(title: nil, message: "El dispositivo no cuenta con una cámara válida.", preferredStyle: .alert)

            let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: {(alert: UIAlertAction!) in
            })

            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)

        }else{
             let vc = UIImagePickerController()
             vc.sourceType = .camera
             vc.allowsEditing = true
             vc.delegate = self
             present(vc, animated: true)
        }
    }
    
    @IBAction func agregarFotoAction(_ sender: Any) {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
    
    @IBAction func agregarDomiciliarioAction(_ sender: Any) {
        let nombre = nombreTextField.text
        let telefono = telefonoTextField.text
        let apellido = apellidoTextField.text
        let foto = previewImageView.image
        if (nombre?.isEmpty == true || telefono?.isEmpty == true || apellido?.isEmpty == true || foto == nil){
            //Crea la alerta
            let alert = UIAlertController(title: "Error",
            message: "Debes llenar todos los campos",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let cancelAction = UIAlertAction(title: "Ok",
              style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            alert.addAction(cancelAction)
            //Muestra la alerta
            present(alert,
            animated: true,
            completion: nil)
        }else{
            guardarDomiciliarioDB(nombre: nombre!, apellido: apellido!, telefono: Int(telefono!)!, foto: createImageThumbnail(foto!))
        }
        
        
    }
    
    //MARK: - Guardar domiciliario en BD
    func guardarDomiciliarioDB(nombre:String, apellido:String, telefono:Int, foto:Data){
        
        //1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let entity = NSEntityDescription.entity(forEntityName: "Domiciliario", in: managedContext)
        let domiciliario = NSManagedObject(entity: entity!, insertInto: managedContext)
        //3
        domiciliario.setValue(nombre, forKey: "nombre")
        domiciliario.setValue(telefono, forKey: "telefono")
        domiciliario.setValue(apellido, forKey: "apellido")
        domiciliario.setValue(foto, forKey: "foto")
        domiciliario.setValue(false, forKey: "enRuta")
        domiciliario.setValue(false, forKey: "guardadoRemoto")
        domiciliario.setValue(usuario, forKey: "usuario")
        //4
        do {
            try managedContext.save()
            //5
            domiciliariosDB.append(domiciliario)
            print("GUARDADO EN BD")
            let guardadoExitoso = UIAlertController(title: "Guardado Exitoso",
            message: "El domiciliario ha sido guardado.",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let ok = UIAlertAction(title: "Ok",
                                       style: .default) { (action: UIAlertAction) -> Void in
                                        self.navigationController?.popViewController(animated: true)
            }
            //Agrega el boton de volver a la alerta
            guardadoExitoso.addAction(ok)
            //Muestra la alerta
            present(guardadoExitoso,
            animated: true,
            completion: nil)
            
        } catch let error as NSError {
        print("No ha sido posible guardar \(error), \(error.userInfo)")
      }
        DispatchQueue.main.async {
            subirDomiciliarios(usuario: self.usuario, context: managedContext)
        }
    }
    
}

// MARK: - TextFields CrearDomiciliario Delegate
extension ViewControllerAgregarDomiciliario: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }

        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == telefonoTextField{
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }else {
            return true
        }
    }
}

