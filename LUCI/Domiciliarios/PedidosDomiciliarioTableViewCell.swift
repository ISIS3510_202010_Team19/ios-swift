//
//  PedidosDomiciliarioTableViewCell.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 16/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit

class PedidosDomiciliarioTableViewCell: UITableViewCell{
    @IBOutlet weak var clienteImageView: UIImageView!
    @IBOutlet weak var nombreClienteLabel: UILabel!
    @IBOutlet weak var horaPedidoLabel: UILabel!
    @IBOutlet weak var entregadoButton: UIButton!
    
    var pedido: Pedido?
    
    var fondo: UIView = {
      let backgroundView = UIView()
      backgroundView.backgroundColor = UIColor.black
      backgroundView.alpha = 0.8
      backgroundView.layer.cornerRadius = 0
      backgroundView.layer.masksToBounds = true
      return backgroundView
    }()
    
    var activityIndicator: UIActivityIndicatorView = {
        var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
      activityIndicator.hidesWhenStopped = true
      activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        activityIndicator.style = UIActivityIndicatorView.Style.large
      activityIndicator.autoresizingMask = [.flexibleLeftMargin , .flexibleRightMargin , .flexibleTopMargin , .flexibleBottomMargin]
      activityIndicator.isUserInteractionEnabled = false
      return activityIndicator
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func pedidoDomiciliario(_ sender: Any) {
        if pedido?.estado == 1{
            let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            pedido!.estado = 2
            do {
                try managedObjectContext.save()
                print("saved!")
                entregadoButton.setImage(UIImage(named: "Tick Icon"), for: .normal)
                print(pedido!.estado)
                
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
    }
    
}
