//
//  DomiciliarioDetailViewController.swift
//  LUCI
//
//  Created by Juan Daniel Ahumada Arcos on 16/04/20.
//  Copyright © 2020 MovG19. All rights reserved.
//

import UIKit

class DomiciliarioDetailViewController: UIViewController, UINavigationControllerDelegate {
    @IBOutlet weak var domiciliarioImageView: UIImageView!
    @IBOutlet weak var nombreDomiciliarioLabel: UILabel!
    @IBOutlet weak var telefonoDomiciliarioLabel: UILabel!
    @IBOutlet weak var numeroPedidosLabel: UILabel!
    
    @IBOutlet weak var pedidosTableView: UITableView!
    
    @IBOutlet weak var enviarDomiciliarioButton: UIButton!
    var domiciliario: Domiciliario?
    var pedidos = [Pedido]()
    var pedidosSinEntregar = [Pedido]()
    
    var fondo: UIView = {
      let backgroundView = UIView()
      backgroundView.backgroundColor = UIColor.black
      backgroundView.alpha = 0.8
      backgroundView.layer.cornerRadius = 0
      backgroundView.layer.masksToBounds = true
      
      return backgroundView
    }()
    
    var activityIndicator: UIActivityIndicatorView = {
        var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
      activityIndicator.hidesWhenStopped = true
      activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        activityIndicator.style = UIActivityIndicatorView.Style.large
      activityIndicator.autoresizingMask = [.flexibleLeftMargin , .flexibleRightMargin , .flexibleTopMargin , .flexibleBottomMargin]
      activityIndicator.isUserInteractionEnabled = false
      return activityIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if domiciliario?.foto != nil{
            let foto = domiciliario!.foto!
            domiciliarioImageView.image = UIImage(data: foto)
            domiciliarioImageView.hideActivityIndicator(activityIndicator: activityIndicator, backgroundView: fondo)
        }else{
            domiciliarioImageView.showActivityIndicator(activityIndicator: activityIndicator, backgroundView: fondo)
        }
        let nombre = domiciliario!.nombre!
        let apellido = domiciliario!.apellido!
        nombreDomiciliarioLabel.text = "\(nombre) \(apellido)"
        telefonoDomiciliarioLabel.text = String(domiciliario!.telefono)
        let numeroPedidos = domiciliario!.pedidos?.count
        numeroPedidosLabel.text = "\(numeroPedidos!)"
        pedidos = domiciliario!.pedidos?.allObjects as! [Pedido]
        for p in pedidos{
            if p.estado != 4{
                pedidosSinEntregar.append(p)
            }
        }
        if pedidosSinEntregar.count == 0 {
            enviarDomiciliarioButton.isEnabled = false
        }
        
        pedidosTableView.tableFooterView = UIView()
        pedidosTableView.dataSource = self
        pedidosTableView.delegate = self
        pedidosTableView.register(UINib(nibName: "PedidosDomiciliarioTableViewCell", bundle: nil), forCellReuseIdentifier: "domiciliarioPedidoTVC")
        }
    
    override func viewWillAppear(_ animated: Bool) {
        pedidosTableView.reloadData()
        var todos2 = false
        if pedidosSinEntregar.count != 0{
            for p in pedidosSinEntregar {
                if p.estado == 3{
                    enviarDomiciliarioButton.setImage(UIImage(named: "Delivered Icon"), for: .normal)
                }
                if p.estado == 2{
                    todos2 = true
                }else{
                    todos2 = false
                }
                
            }
        }
        if todos2 == true{
            enviarDomiciliarioButton.setImage(UIImage(named: "Enviar Icon"), for: .normal)
        }
        let tutorialCompletado = UserDefaults.standard.bool(forKey: "tutorialCompletado")
        let estadoTuto = UserDefaults.standard.integer(forKey: "tutoCount")
        if tutorialCompletado == false{
            if estadoTuto == 4{
                UserDefaults.standard.set(5, forKey:"tutoCount")
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Presionando el botón al lado de los pedidos puedes indicarnos que este pedido ya lo tiene el domiciliario. Para continuar con el estado de los pedidos debes enviar al domiciliario presionando el botón azul con el avión de papel. Ten en cuenta que para que el domiciliario pueda salir hacia el destino debe tener todos los pedidos asignados por LUCI.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            } else if estadoTuto == 8{
                UserDefaults.standard.set(10, forKey:"tutoCount")
                let tutorial = UIAlertController(title: "Tutorial",
                message: "Presiona el botón del camión para indicar que el domiciliario ya regresó y para marcar como finalizado el pedido.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                }
                //Agrega el boton de volver a la alerta
                tutorial.addAction(ok)
                //Muestra la alerta
                present(tutorial,
                animated: true,
                completion: nil)
            }
            
        }
    }
    
    
    @IBAction func enviarDomiciliario(_ sender: Any) {
        var sePuedeEnviar = true
        for p in pedidosSinEntregar{
            if p.estado != 2{
                sePuedeEnviar = false
            }
        }
        if domiciliario?.enRuta == true{
            enviarDomiciliarioButton.setImage(UIImage(named: "Tick Icon"), for: .normal)
            for p in pedidosSinEntregar{
                p.estado = 4
            }
            domiciliario?.enRuta = false
            let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            do {
                try managedObjectContext.save()
                let domiciliarioEnCamino = UIAlertController(title: "Domicilo Finalizado",
                message: "Se reportó la vuelta de tu domiciliario.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                                            self.navigationController?.popViewController(animated: true)
                }
                //Agrega el boton de volver a la alerta
                domiciliarioEnCamino.addAction(ok)
                //Muestra la alerta
                present(domiciliarioEnCamino,
                animated: true,
                completion: nil)
                
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
            
            
            
        }
        if sePuedeEnviar == true && domiciliario?.enRuta == false{
            enviarDomiciliarioButton.setImage(UIImage(named: "Delivered Icon"), for: .normal)
            let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            domiciliario?.enRuta = true
            for p in pedidosSinEntregar{
                p.estado = 3
            }
            do {
                try managedObjectContext.save()
                let domiciliarioEnCamino = UIAlertController(title: "Domiciliario en camino",
                message: "Tu domiciliario ya va en camino con los pedidos para tus clientes.",
                preferredStyle: .alert)
                
                //Creamos el UIAlertAction que nos permitirá volver
                let ok = UIAlertAction(title: "Ok",
                                           style: .default) { (action: UIAlertAction) -> Void in
                                            self.navigationController?.popViewController(animated: true)
                }
                //Agrega el boton de volver a la alerta
                domiciliarioEnCamino.addAction(ok)
                //Muestra la alerta
                present(domiciliarioEnCamino,
                animated: true,
                completion: nil)
                
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        }else{
            //Crea la alerta
            let alert = UIAlertController(title: "Error",
            message: "El domiciliario aún no tiene todos los pedidos que tiene que entregar.",
            preferredStyle: .alert)
            
            //Creamos el UIAlertAction que nos permitirá volver
            let cancelAction = UIAlertAction(title: "Ok",
              style: .default) { (action: UIAlertAction) -> Void in
            }
            //Agrega el boton de volver a la alerta
            alert.addAction(cancelAction)
            //Muestra la alerta
            present(alert,
            animated: true,
            completion: nil)
        }
        
        
    }
    
    
}

extension DomiciliarioDetailViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pedidosSinEntregar.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "domiciliarioPedidoTVC") as? PedidosDomiciliarioTableViewCell
        let cliente = pedidosSinEntregar[indexPath.row].cliente
        cell?.pedido = pedidosSinEntregar[indexPath.row]
        cell?.nombreClienteLabel.text = cliente?.nombre
        cell?.horaPedidoLabel.text = "Para las 11:00am"
        if (cliente?.foto != nil){
            cell?.clienteImageView.image = UIImage(data: (cliente?.foto)!)
            cell?.clienteImageView.hideActivityIndicator(activityIndicator: cell!.activityIndicator, backgroundView: cell!.fondo)
        }else{
            cell?.clienteImageView.showActivityIndicator(activityIndicator: cell!.activityIndicator, backgroundView: cell!.fondo)
        }
        
        if pedidosSinEntregar[indexPath.row].estado == 2 || pedidosSinEntregar[indexPath.row].estado == 3{
            cell?.entregadoButton.setImage(UIImage(named: "Tick Icon"), for: .normal)
            cell?.entregadoButton.isEnabled = false
        }
        return cell!
        
        
    }
    
    
}

extension DomiciliarioDetailViewController: UITableViewDelegate{
    
}
